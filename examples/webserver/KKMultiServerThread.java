import java.net.*;
import java.io.*;

public class KKMultiServerThread extends Thread {
    private Socket socket = null;

    public KKMultiServerThread(Socket socket) {
	super("KKMultiServerThread");
	this.socket = socket;
    }

    public void run() {

	try {
		Process child = null;
	    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
	    BufferedReader in = new BufferedReader(
				    new InputStreamReader(
				    socket.getInputStream()));

	    String inputLine, outputLine;

		out.println("<head>");
		out.println("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
		out.println("<title>ATMPING</title>");
		out.println("</head><body><pre>");

		try {
			String [] cmdLine=new String [3];
			cmdLine[0]="cmd.exe";
			cmdLine[1]="/C";
                        cmdLine[2]="d:\\debug\\atmping.bat n";

        	child = Runtime.getRuntime().exec(cmdLine);
	     	BufferedReader input = new BufferedReader (new InputStreamReader(child.getInputStream()));
			while ((outputLine = input.readLine()) != null) {
	    		out.println(outputLine);
			}
     		input.close();
     	}
   		 catch (Exception err) {
    		 err.printStackTrace();
     	}

		outputLine = "</pre></body></html>";
	    out.println(outputLine);

	    out.close();
	    in.close();
	    socket.close();

	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
}
