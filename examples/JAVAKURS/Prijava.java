import java.awt.*;

class Prijava extends Panel {
     public Prijava() {

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.insets = new Insets(1,1,1,1);
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;  
        gbc.fill = GridBagConstraints.NONE; 

        Label naslov = new Label("Prijava korisnika");
        gbc.anchor = GridBagConstraints.NORTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag.setConstraints(naslov, gbc);
        add(naslov);

        Label tekstIme = new Label("Ime polaznika");
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag.setConstraints(tekstIme, gbc);
        add(tekstIme);

        TextField poljeIme = new TextField(20);
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag.setConstraints(poljeIme, gbc);
        add(poljeIme);

        Label tekstLozinka = new Label("Lozinka");
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag.setConstraints(tekstLozinka, gbc);
        add(tekstLozinka);

        TextField poljeLozinka = new TextField(20);
        poljeLozinka.setEchoCharacter('*');
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag.setConstraints(poljeLozinka, gbc);
        add(poljeLozinka);

        Button prijava = new Button("Prijava");
        gbc.anchor = GridBagConstraints.SOUTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag.setConstraints(prijava, gbc);
        add(prijava);
   }
}
