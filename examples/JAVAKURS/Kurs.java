import java.applet.Applet;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.lang.*;

public class Kurs extends Applet {
    Lekcija l1;
    public void init() {
        OtvorenURL url = new OtvorenURL(getCodeBase(), getParameter("lekcija"));
        UcitanFajl uf = new UcitanFajl(url.getOtvorenURL());
        l1 = new Lekcija(this, uf);

        setLayout(new BorderLayout());
        setFont(new Font("Helvetica", Font.PLAIN, 14));

        //Put the Choice in a Panel to get a nicer look.
        Panel cp = new Panel();
        Choice c = new Choice();
        for (int i = 0; i < l1.dajBrojStranica() ; i++)
             c.addItem(l1.dajId(i));
        cp.add(c);
        add("North", cp);

        add("Center", l1);
        validate();
    }

    public Insets insets() {
        return new Insets(4,4,5,5);
    }

    public void paint(Graphics g) {
        Dimension d = size();
        Color bg = getBackground();

        g.setColor(bg);
        g.draw3DRect(0, 0, d.width - 1, d.height - 1, true);
        g.draw3DRect(3, 3, d.width - 7, d.height - 7, false);
    }

    public boolean action(Event evt, Object arg) {
        if (evt.target instanceof Choice) {
            ((CardLayout)l1.getLayout()).show(l1,(String)arg);
            return true;
        }
        return false;
    }
}
