import java.awt.*;
import java.util.*;

public class TestPojmovi extends Panel {
    TextField[] odgovor = new TextField[10];
    public TestPojmovi(String poj[]) {
        Label pojam;
        Panel cbp = new Panel();
        cbp.setLayout(new GridLayout(0, 2));
        for (int i = 0; i < poj.length; i++) {
             pojam = new Label(poj[i]);
             cbp.add(pojam);
             odgovor[i] = new TextField(20);
             cbp.add(odgovor[i]);
        }

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gridbag);
        gbc.insets = new Insets(4,4,5,5);
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;  
        gbc.fill = GridBagConstraints.NONE; 
        gbc.anchor = GridBagConstraints.NORTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag.setConstraints(cbp, gbc);
        add(cbp);

        validate();
    }
}
