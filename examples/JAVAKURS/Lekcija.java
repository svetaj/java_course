import java.applet.Applet;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.lang.*;

class Lekcija extends Panel {
    String[] id;
    int brojStranica;

    public Lekcija(Applet app, UcitanFajl ucitanFajl) {
        setLayout(new CardLayout());

        brojStranica = ucitanFajl.brojLinijaFajla()/2;
        ucitanFajl.naPocetakFajla();

        Naslov nasl;
        id = new String[100];
        String fajl;
        int brojac;
        Image gif;
        Panel[] p = new Panel[4];
        Stranica stranica;

        UcitanFajl uf;
        OtvorenURL url;

        for (int i=0; i < brojStranica; i++) {
               brojac = 0;
               id[i] = ucitanFajl.sledecaLinijaFajla();
               System.out.println(id[i]);
               nasl = new Naslov(id[i]);
               StringTokenizer st = new StringTokenizer(
                                      ucitanFajl.sledecaLinijaFajla());
               try {
               while( (fajl = st.nextToken()) != null) {
                    System.out.println(fajl);
                    url = new OtvorenURL(app.getCodeBase(), fajl);
                    if (fajl.endsWith(".sli")) {
                         gif = app.getImage(url.getOtvorenURL());
                         p[brojac] = new Panel();
                         p[brojac].add(new Slika(gif, 530, 180));
                    }
                    else {
                         uf = new UcitanFajl(url.getOtvorenURL());
                         if (fajl.endsWith(".tek"))
                              p[brojac] = new Tekst(uf);
                         else if (fajl.endsWith(".teo"))
                              p[brojac] = new Teorija(uf);
                         else if (fajl.endsWith(".pri"))
                              p[brojac] = new Primer(uf);
                         else if (fajl.endsWith(".crt"))
                              p[brojac] = new Crtez(uf);
                    }
                    brojac++;
               }
               } catch (NoSuchElementException e) {
               }
               switch(brojac) {
               case 1:
                    stranica = new Stranica(nasl, p[0]);
                    break;
               case 2:
                    stranica = new Stranica(nasl, p[0], p[1]);
                    break;
               case 3:
                    stranica = new Stranica(nasl, p[0], p[1], p[2]);
                    break;
               case 4:
                    stranica = new Stranica(nasl, p[0], p[1], p[2], p[3]);
                    break;
               default:
                    stranica = null;
               }
               add(id[i], stranica);
        }

    }
    int dajBrojStranica()
    {
         return brojStranica;
    }

    String dajId(int i)
    {
         if (i >= 0 && i < brojStranica)
               return id[i];
         return id[0];
    }
}
