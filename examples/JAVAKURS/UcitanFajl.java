import java.io.*;
import java.net.*;
import java.util.*;

class UcitanFajl {
    URL ucitan_url;
    final static int MAXLIN = 200;
    String[] linija;
    int brlin;
    int tekuca;

    public UcitanFajl(URL url)
    {
          linija = new String[MAXLIN];
          brlin = 0;
          tekuca = 0;
          DataInputStream dis = null;
          try {
               dis = new DataInputStream(url.openStream());
               while (brlin >= MAXLIN ||
                      (linija[brlin] = dis.readLine()) != null) 
                    brlin++;
               dis.close();
               ucitan_url = url;
          } catch (IOException e) {
               System.out.println("Greska - fajl: " + e);
               brlin = 0;
          }
    }
    public String sledecaLinijaFajla() {
         if (tekuca < brlin) 
             return linija[tekuca++];
         else
             return null;
    }
    public void naPocetakFajla() {
         tekuca = 0;
    }
    public int brojLinijaFajla() {
         return brlin;
    }
}
