import java.awt.*;

class Naslov extends Panel {
    public Naslov(String linija) {
        Label lin;
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.weightx = 0.0;                  
        gbc.weighty = 0.0;                 
        gbc.fill = GridBagConstraints.NONE; 
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda

        lin = new Label(linija);
        gridbag.setConstraints(lin, gbc);
        add(lin);
    }
}
