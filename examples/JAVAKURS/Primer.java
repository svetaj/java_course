import java.awt.*;

class Primer extends Panel {
     public Primer(String[] tekst) {

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        TextArea ta = new TextArea();
        for (int i = 0; i < tekst.length; i++)
             ta.appendText(tekst[i]+"\n");
        ta.setEditable(false);
        gridbag.setConstraints(ta, gbc);
        add(ta);
   }
     public Primer(UcitanFajl ucitanFajl) {

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        TextArea ta = new TextArea();
        ucitanFajl.naPocetakFajla();
        for (int i = 0; i < ucitanFajl.brojLinijaFajla(); i++)
             ta.appendText(ucitanFajl.sledecaLinijaFajla()+"\n");
        ta.setEditable(false);
        gridbag.setConstraints(ta, gbc);
        add(ta);
   }
}
