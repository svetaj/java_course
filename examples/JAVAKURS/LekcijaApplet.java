import java.applet.Applet;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.lang.*;

public class LekcijaApplet extends Applet {
    Lekcija l1;
    Button napred, nazad, kraj, sadrzaj;
    int strana;

    public void init() {
        OtvorenURL url = new OtvorenURL(getCodeBase(), getParameter("lekcija"));
        UcitanFajl uf = new UcitanFajl(url.getOtvorenURL());
        l1 = new Lekcija(this, uf);
        strana = 0;

        setLayout(new BorderLayout());
        setFont(new Font("Helvetica", Font.PLAIN, 14));

        Panel cp = new Panel();
        napred = new Button("napred");
        cp.add(napred);
        nazad = new Button("nazad");
        cp.add(nazad);
        sadrzaj = new Button("sadrzaj");
        cp.add(sadrzaj);
        kraj = new Button("kraj");
        cp.add(kraj);
        add("South", cp);

        add("Center", l1);
        validate();
    }

    public Insets insets() {
        return new Insets(4, 4, 5, 5);
    }

    public void paint(Graphics g) {
        Dimension d = size();
        Color bg = getBackground();

        g.setColor(bg);
        g.draw3DRect(0, 0, d.width - 1, d.height - 1, true);
        g.draw3DRect(3, 3, d.width - 7, d.height - 7, false);
    }

    public boolean action(Event evt, Object arg) {
        if(evt.target == napred || evt.target == nazad) {
            if (evt.target == napred)
                strana++;
            else if (evt.target == nazad)
                strana--;
            ((CardLayout)l1.getLayout()).show(l1, l1.dajId(strana));
            if(strana == l1.dajBrojStranica() - 1)
                 napred.setEnabled(false);
            else if(strana < l1.dajBrojStranica() - 1)
                 napred.setEnabled(true);
            if(strana == 0)
                 nazad.setEnabled(false);
            else if(strana > 0)
                 nazad.setEnabled(true);
            return true;
        }
        return false;
    }
}
