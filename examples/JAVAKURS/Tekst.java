import java.awt.*;

class Tekst extends Panel {
     public Tekst(String[] tekst) {

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        TextArea ta = new TextArea("", 0, 0, TextArea.SCROLLBARS_NONE);
        for (int i = 0; i < tekst.length; i++)
             ta.appendText(tekst[i]+"\n");
        ta.setEditable(false);
        ta.setEnabled(false);
        Font f = new Font("Serif", Font.BOLD | Font.ITALIC, 14);
        ta.setFont(f);
        gridbag.setConstraints(ta, gbc);
        add(ta);
   }
     public Tekst(UcitanFajl ucitanFajl) {

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        TextArea ta = new TextArea("", 0, 0, TextArea.SCROLLBARS_NONE);
        ucitanFajl.naPocetakFajla();
        for (int i = 0; i < ucitanFajl.brojLinijaFajla(); i++)
             ta.appendText(ucitanFajl.sledecaLinijaFajla()+"\n");
        ta.setEditable(false);
        ta.setEnabled(false);
        Font f = new Font("Serif", Font.BOLD | Font.ITALIC, 14);
        ta.setFont(f);
        gridbag.setConstraints(ta, gbc);
        add(ta);
   }
}
