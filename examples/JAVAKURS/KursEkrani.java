import java.applet.Applet;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.lang.*;

public class KursEkrani extends Applet {
    Panel cards;
    final static String BUTTONPANEL = "Panel with Buttons";
    final static String PRIJAVA = "Panel sa prijavom polaznika";
    final static String PRIMEROPIS = "Panel sa primerom i opisom";
    final static String SAMOCRTEZ = "Panel samo sa crtezom";
    final static String TEORIJA = "Panel sa gif-om i primerom 1i2x2";
    final static String TEORIJA1 = "Panel samo sa teorijom";
    final static String CRTEZTEORIJA = "Panel sa crtezom i teorijom";
    final static String GIFTEORIJA = "Panel sa gif i teorijom";
    final static String REGISTRACIJA = "Panel sa registracijom polaznika";
    final static String TESTIZBOR = "Panel sa testom - izbor odgovora";
    final static String TESTPAROVI = "Panel sa testom - parovi odgovora";
    final static String TESTPOJMOVI = "Panel sa testom - pojmovi";
    final static String FORMTEKST = "Panel sa formatizovanim tekstom";

    String[] text1 = { "LINE", "RECT", "TEXT", "MATR"};
    String[] text2 = { "LINE", "RECT", "TEXT", "MATR",
                       "NETW", "ARRO", "POIN", "PARR",
                       "STRA", "FONT", "CLEA",
                       "LOOP", "SLOW"};
    public void init() {

        OtvorenURL url;

        setLayout(new BorderLayout());
        setFont(new Font("Helvetica", Font.PLAIN, 14));

        //Put the Choice in a Panel to get a nicer look.
        Panel cp = new Panel();
        Choice c = new Choice();
        c.addItem(BUTTONPANEL);
        c.addItem(PRIJAVA);
        c.addItem(PRIMEROPIS);
        c.addItem(SAMOCRTEZ);
        c.addItem(TEORIJA);
        c.addItem(TEORIJA1);
        c.addItem(CRTEZTEORIJA);
        c.addItem(GIFTEORIJA);
        c.addItem(REGISTRACIJA);
        c.addItem(TESTIZBOR);
        c.addItem(TESTPAROVI);
        c.addItem(TESTPOJMOVI);
        c.addItem(FORMTEKST);
        cp.add(c);
        add("North", cp);

        cards = new Panel();
        cards.setLayout(new CardLayout());

//------------------- PANEL P1 -------------------------------------   
        Panel p1 = new Panel();
        p1.add(new Button("Button 1"));
        p1.add(new Button("Button 2"));
        p1.add(new Button("Button 3"));

//------------------- PANEL P2 -------------------------------------   
        Prijava p2 = new Prijava();

//------------------- PANEL P3 -------------------------------------   
        Naslov nas3 = new Naslov("Ovo je naslov");
        url = new OtvorenURL(getCodeBase(), "FILE:trt.mrt");
        UcitanFajl uf3 = new UcitanFajl(url.getOtvorenURL());
        Primer pri3 = new Primer(uf3);
        Teorija teo3 = new Teorija(text1);
        Stranica p3 = new Stranica(nas3, pri3, teo3);

//------------------- PANEL P4 -------------------------------------
        url = new OtvorenURL(getCodeBase(), "FILE:trt.mrt");
        UcitanFajl uf4 = new UcitanFajl(url.getOtvorenURL());
        Crtez p4 = new Crtez(uf4);

//------------------- PANEL P5 -------------------------------------   
        Naslov nas5 = new Naslov("Ovo je naslov");
        url = new OtvorenURL(getCodeBase(), "FILE:trt.gif");
        Image gifSlika = getImage(url.getOtvorenURL());
        Slika sli5 = new Slika(gifSlika, 530, 180);
        Teorija teo5 = new Teorija(text1);
        Primer pri5 = new Primer(text2);
        Naslov nas55 = new Naslov("Jos jedan naslov");
        Stranica p5 = new Stranica(nas5, sli5, teo5, pri5, nas55);

//------------------- PANEL P6 -------------------------------------
        url = new OtvorenURL(getCodeBase(), "FILE:trt.mrt");
        UcitanFajl uf6 = new UcitanFajl(url.getOtvorenURL());
        Teorija p6 = new Teorija(uf6);

//------------------- PANEL P7 -------------------------------------   
        Naslov nas7 = new Naslov("Ovo je naslov");
        url = new OtvorenURL(getCodeBase(), "FILE:trt1.mrt");
        UcitanFajl uf7 = new UcitanFajl(url.getOtvorenURL());
        Crtez crt7 = new Crtez(uf7);
        Teorija teo7 = new Teorija(text1);
        Stranica p7 = new Stranica(nas7, crt7, teo7);

//------------------- PANEL P8 -------------------------------------   
        Naslov nas8 = new Naslov("Ovo je naslov");
        Slika sli8 = new Slika(gifSlika, 530, 180);
        Teorija teo8 = new Teorija(text1);
        Stranica p8 = new Stranica(nas8, sli8, teo8);

//------------------- PANEL P9 -------------------------------------   
        Registracija p9 = new Registracija();

//------------------- PANEL P10 -------------------------------------   
        TestJedanOd p10 = new TestJedanOd(text1);

//------------------- PANEL P11 -------------------------------------   
        TestParovi p11 = new TestParovi(text1, text1);

//------------------- PANEL P12 -------------------------------------   
        TestPojmovi p12 = new TestPojmovi(text1);

//------------------- PANEL P13 -------------------------------------
        url = new OtvorenURL(getCodeBase(), "FILE:Tekst.java");
        UcitanFajl uf13 = new UcitanFajl(url.getOtvorenURL());
        Tekst p13 = new Tekst(uf6);

//------------------- SVI PANELI -----------------------------------
        cards.add(BUTTONPANEL, p1);
        cards.add(PRIJAVA, p2);
        cards.add(PRIMEROPIS, p3);
        cards.add(SAMOCRTEZ, p4);
        cards.add(TEORIJA, p5);
        cards.add(TEORIJA1, p6);
        cards.add(CRTEZTEORIJA, p7);
        cards.add(GIFTEORIJA, p8);
        cards.add(REGISTRACIJA, p9);
        cards.add(TESTIZBOR, p10);
        cards.add(TESTPAROVI, p11);
        cards.add(TESTPOJMOVI, p12);
        cards.add(FORMTEKST, p13);
        add("Center", cards);
        validate();
    }

    public Insets insets() {
        return new Insets(4,4,5,5);
    }

    public void paint(Graphics g) {
        Dimension d = size();
        Color bg = getBackground();

        g.setColor(bg);
        g.draw3DRect(0, 0, d.width - 1, d.height - 1, true);
        g.draw3DRect(3, 3, d.width - 7, d.height - 7, false);
    }

    public boolean action(Event evt, Object arg) {
        if (evt.target instanceof Choice) {
            ((CardLayout)cards.getLayout()).show(cards,(String)arg);
            return true;
        }
        return false;
    }
}
