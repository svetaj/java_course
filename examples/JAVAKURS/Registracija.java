import java.awt.*;

class Registracija extends Panel {
     public Registracija() {

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.insets = new Insets(2,2,2,2);
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;  
        gbc.fill = GridBagConstraints.NONE; 

        Label naslov = new Label("Registracija korisnika");
        gbc.anchor = GridBagConstraints.NORTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag.setConstraints(naslov, gbc);
        add(naslov);

//-----------------Levi panel ------------------------------------------
        Panel levo = new Panel();

        GridBagLayout gridbag1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();

        gbc1.insets = new Insets(2,2,2,2);
        gbc1.weightx = 0.0;
        gbc1.weighty = 0.0;  
        gbc1.fill = GridBagConstraints.NONE; 
        gbc1.anchor = GridBagConstraints.WEST;

        levo.setLayout(gridbag1);

        Label tekstIme = new Label("Ime               ");
        gbc1.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag1.setConstraints(tekstIme, gbc1);
        levo.add(tekstIme);

        TextField poljeIme = new TextField(20);
        gbc1.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag1.setConstraints(poljeIme, gbc1);
        levo.add(poljeIme);

        Label tekstLozinka = new Label("Lozinka           ");
        gbc1.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag1.setConstraints(tekstLozinka, gbc1);
        levo.add(tekstLozinka);

        TextField poljeLozinka = new TextField(20);
        poljeLozinka.setEchoCharacter('*');
        gbc1.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag1.setConstraints(poljeLozinka, gbc1);
        levo.add(poljeLozinka);

        Label tekstLozinka1 = new Label("Ponovljena Lozinka");
        gbc1.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag1.setConstraints(tekstLozinka1, gbc1);
        levo.add(tekstLozinka1);

        TextField poljeLozinka1 = new TextField(20);
        poljeLozinka1.setEchoCharacter('*');
        gbc1.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag1.setConstraints(poljeLozinka1, gbc1);
        levo.add(poljeLozinka1);

        Label tekstMail = new Label("E-mail            ");
        gbc1.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag1.setConstraints(tekstMail, gbc1);
        levo.add(tekstMail);

        TextField poljeMail = new TextField(20);
        gbc1.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag1.setConstraints(poljeMail, gbc1);
        levo.add(poljeMail);

//------------- Desni panel --------------------------------------------
        Panel desno = new Panel();

        GridBagLayout gridbag2 = new GridBagLayout();
        GridBagConstraints gbc2 = new GridBagConstraints();

        gbc2.insets = new Insets(2,2,2,2);
        gbc2.weightx = 0.0;
        gbc2.weighty = 0.0;  
        gbc2.fill = GridBagConstraints.NONE; 
        gbc2.anchor = GridBagConstraints.WEST;

        desno.setLayout(gridbag2);

        Label tekstUlica = new Label("Ulica             ");
        gbc2.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag2.setConstraints(tekstUlica, gbc2);
        desno.add(tekstUlica);

        TextField poljeUlica = new TextField(20);
        gbc2.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag2.setConstraints(poljeUlica, gbc2);
        desno.add(poljeUlica);

        Label tekstUlica1 = new Label("Ulica (nastavak)  ");
        gbc2.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag2.setConstraints(tekstUlica1, gbc2);
        desno.add(tekstUlica1);

        TextField poljeUlica1 = new TextField(20);
        gbc2.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag2.setConstraints(poljeUlica1, gbc2);
        desno.add(poljeUlica1);

        Label tekstMesto = new Label("Mesto             ");
        gbc2.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag2.setConstraints(tekstMesto, gbc2);
        desno.add(tekstMesto);

        TextField poljeMesto = new TextField(20);
        gbc2.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag2.setConstraints(poljeMesto, gbc2);
        desno.add(poljeMesto);

        Label tekstBroj = new Label("Postanski broj    ");
        gbc2.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag2.setConstraints(tekstBroj, gbc2);
        desno.add(tekstBroj);

        TextField poljePbroj = new TextField(20);
        gbc2.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag2.setConstraints(poljePbroj, gbc2);
        desno.add(poljePbroj);

        Label tekstTelefon = new Label("Telefon           ");
        gbc2.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag2.setConstraints(tekstTelefon, gbc2);
        desno.add(tekstTelefon);

        TextField poljeTelefon = new TextField(20);
        gbc2.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag2.setConstraints(poljeTelefon, gbc2);
        desno.add(poljeTelefon);
//----------------------------------------------------------------------

        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag.setConstraints(levo, gbc);
        add(levo);

        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag.setConstraints(desno, gbc);
        add(desno);

        Button posalji = new Button("Posalji");
        gbc.anchor = GridBagConstraints.EAST;
        gbc.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gridbag.setConstraints(posalji, gbc);
        add(posalji);

        Button prekini = new Button("Prekini");
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag.setConstraints(prekini, gbc);
        add(prekini);
   }
}
