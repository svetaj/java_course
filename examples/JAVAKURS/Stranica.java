import java.awt.*;

class Stranica extends Panel {
     public Stranica(Component c1, Component c2, Component c3, Component c4, Component c5) {

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.weightx = 0.0;                   //reset to the default
        gbc.weighty = 0.0;                   //reset to the default
        gbc.fill = GridBagConstraints.NONE; 
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda

        gridbag.setConstraints(c1, gbc);
        add(c1);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        gridbag.setConstraints(c2, gbc);
        add(c2);

        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        gridbag.setConstraints(c3, gbc);
        add(c3);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        gridbag.setConstraints(c4, gbc);
        add(c4);

        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        gridbag.setConstraints(c5, gbc);
        add(c5);
   }

     public Stranica(Component c1, Component c2, Component c3, Component c4) {

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.weightx = 0.0;                   //reset to the default
        gbc.weighty = 0.0;                   //reset to the default
        gbc.fill = GridBagConstraints.NONE; 
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda

        gridbag.setConstraints(c1, gbc);
        add(c1);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.RELATIVE; //pretposlednji
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        gridbag.setConstraints(c2, gbc);
        add(c2);

        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        gridbag.setConstraints(c3, gbc);
        add(c3);

        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        gridbag.setConstraints(c4, gbc);
        add(c4);
   }


     public Stranica(Component c1, Component c2, Component c3) {

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.weightx = 0.0;                   //reset to the default
        gbc.weighty = 0.0;                   //reset to the default
        gbc.fill = GridBagConstraints.NONE; 
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda

        gridbag.setConstraints(c1, gbc);
        add(c1);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        gridbag.setConstraints(c2, gbc);
        add(c2);

        gbc.weightx = 0.0;                   //reset to the default
        gbc.weighty = 0.0;                   //reset to the default
        gbc.fill = GridBagConstraints.NONE; 
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda

        gridbag.setConstraints(c3, gbc);
        add(c3);
   }


     public Stranica(Component c1, Component c2) {

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.weightx = 0.0;                   //reset to the default
        gbc.weighty = 0.0;                   //reset to the default
        gbc.fill = GridBagConstraints.NONE; 
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda

        gridbag.setConstraints(c1, gbc);
        add(c1);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gbc.weightx = 1.0; 
        gbc.weighty = 1.0; 

        gridbag.setConstraints(c2, gbc);
        add(c2);
   }


     public Stranica(Component c1) {

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.weightx = 0.0;                   //reset to the default
        gbc.weighty = 0.0;                   //reset to the default
        gbc.fill = GridBagConstraints.NONE; 
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda

        gridbag.setConstraints(c1, gbc);
        add(c1);
   }

}
