import java.awt.*;
import java.util.*;

public class Crtez extends Panel {
    UcitanFajl ucitanFajl;
    int dx, dy, s1, s2;
    final static int maxCharHeight = 15;
    final static String[] primitiva = { "LINE", "RECT", "TEXT", "MATR",
                                        "NETW", "ARRO", "POIN", "PARR",
                                        "STRA", "FONT", "CLEA", "DIME",
                                        "SCAL", "LOOP", "SLOW", "#"};
    final static int brpr = 16;

    public Crtez(UcitanFajl uf) {
         BorderLayout b = new BorderLayout();
         setLayout(b);
         ucitanFajl = uf;
         s1 = 1;
         s2 = 1;
         dx = 0;
         dy = 0;
    }

    public void paint(Graphics g)
    {
         int i, j, brojac = 0, prva = 0, pauza = 0;

         ucitanFajl.naPocetakFajla();
         for (i = 0; i < ucitanFajl.brojLinijaFajla(); i++) {
             brojac = intlin(g, ucitanFajl.sledecaLinijaFajla());
             if (brojac > 0) {
                 prva = i;
                 break;
             } else if (brojac < 0) {
                 pauza = brojac;
                 brojac = 0;
                 try {
                      Thread.sleep(-pauza*1000);
                 } catch (InterruptedException e) { }
             }
         }
         for (j = 0; j < brojac; j++) {
             for (i = prva; i < ucitanFajl.brojLinijaFajla(); i++) {
                 pauza = intlin(g, ucitanFajl.sledecaLinijaFajla());
                 if (pauza < 0)
                      try {
                          Thread.sleep(-pauza*1000);
                      } catch (InterruptedException e) { }
             }
         }
    }

    int intlin(Graphics g, String linija)
    {
        StringTokenizer st = new StringTokenizer(linija);
        int[] x = new int[20];
        int j, npar, indpri = -1;
        String temp = st.nextToken();
        Integer i;

        for(j = 0; j < brpr; j++) {
             if (temp.startsWith(primitiva[j])) {
                  indpri = j;
                  break;
             }
        }

        if (indpri == 15)
            return 0;

        if (indpri == 8) {
            for(npar = 0; npar < 5; npar++) {
                 i = new Integer(st.nextToken());
                 x[npar] = i.intValue();
            }
            for (j = 0; j < x[4]; j++) {
                 x[0] += x[2];
                 x[1] += x[3];
                 temp = st.nextToken();
                 drawlin(g, 2, x, temp);
            }
            return 0;
        }

        if (indpri == 2 || indpri == 6 || indpri == 7 || indpri == 9) {
            temp = st.nextToken();
        }

        for(npar = 0; st.hasMoreTokens(); npar++) {
           i = new Integer(st.nextToken());
           x[npar] = i.intValue();
        }
        j = drawlin(g, indpri, x, temp);
        return j;
    }


    int drawlin(Graphics g, int indpri, int[] x, String temp)
    {
        Integer i;
        int j;
        int[] y = new int[10];

        switch(indpri) {
        case 0:  // LINIJA
             drawL(g,x[0], x[1], x[2], x[3]);
             break;
        case 1:  // PRAVOUGAONIK
             drawR(g,x[0], x[1], x[2], x[3]);
             break;
        case 2:  // NIZ KARAKTERA
             drawS(g,temp, x[0], x[1]);
             break;
        case 3:  // MATRICA (SA NUMERACIJOM)
             if (x[4] > 1) 
                  for (j = 1; j <= x[4]; j++) {
                      i = new Integer(j-1); 
                      drawS(g,i.toString(), x[0]-x[2]/2+x[2]*j,
                                                 x[1]-x[7]);
                  }
             if (x[5] > 1) 
                  for (j = 1; j <= x[5]; j++) { 
                      i = new Integer(j-1); 
                      drawS(g,i.toString(), x[0]-x[6],
                                                 x[1]-x[3]/2+x[3]*j);
                  }
        case 4:  // MREZA
             drawR(g,x[0], x[1], x[2]*x[4], x[3]*x[5]);
             for (j = 1; j < x[4]; j++) 
                  drawL(g,x[0]+x[2]*j, x[1], x[0]+x[2]*j, x[1]+x[3]*x[5]);
             for (j = 1; j < x[5]; j++) 
                  drawL(g,x[0], x[1]+x[3]*j, x[0]+x[2]*x[4], x[1]+x[3]*j);
             break;
        case 5:  // STRELICA
             drawL(g,x[0], x[1], x[2], x[3]);
             switch (x[5]) {
             case 0:
                  drawL(g,x[2]-x[4]/2, x[3]+x[4]/4, x[2], x[3]);
                  drawL(g,x[2]-x[4]/2, x[3]-x[4]/4, x[2], x[3]);
                  if (x[6] > 0)
                      fillO(g,x[0]-x[6]/2, x[1]-x[6]/2, x[6], x[6]); 
                  break;
             case 90:
                  drawL(g,x[2]-x[4]/4, x[3]+x[4]/2, x[2], x[3]);
                  drawL(g,x[2]+x[4]/4, x[3]+x[4]/2, x[2], x[3]);
                  if (x[6] > 0)
                      fillO(g,x[0]-x[6]/2, x[1]-x[6]/2, x[6], x[6]); 
                  break;
             case 180:
                  drawL(g,x[2]+x[4]/2, x[3]+x[4]/4, x[2], x[3]);
                  drawL(g,x[2]+x[4]/2, x[3]-x[4]/4, x[2], x[3]);
                  if (x[6] > 0)
                      fillO(g,x[0]-x[6]/2, x[1]-x[6]/2, x[6], x[6]); 
                  break;
             case 270:
                  drawL(g,x[2]-x[4]/4, x[3]-x[4]/2, x[2], x[3]);
                  drawL(g,x[2]+x[4]/4, x[3]-x[4]/2, x[2], x[3]);
                  if (x[6] > 0)
                      fillO(g,x[0]-x[6]/2, x[1]-x[6]/2, x[6], x[6]); 
                  break;
             };
             break;
        case 6:  // POINTER
             drawlin(g, 1, x, temp);      // PRAVOUGAONIK
             y[0] = x[0] + x[2] + x[4];        // donji levi ugao x
             y[1] = x[1];                      //                 y
             y[2] = x[2];                      // DX
             y[3] = x[3];                      // DY
             y[4] = x[7];                      // NX
             y[5] = 1;                         // NY
             y[6] = 0;                         // rastojanje numeracije
             y[7] = x[6];                      // rastojanje numeracije
             if (!temp.startsWith("NONE")) 
                  drawlin(g, 3, y, temp);      // MATRICA SA NUMERACIJOM
             else
                  drawlin(g, 4, y, temp);      // MREZA LINIJA
             y[0] = x[0]+x[2]/2;               // pocetak x
             y[1] = x[1]+x[3]/2;               //         y
             y[2] = x[0]+x[2]+x[4];            // kraj    x
             y[3] = x[1]+x[3]/2;               //         y
             y[4] = x[3]/4;                    // dimenzija strelice
             y[5] = 0;                         // ugao
             y[6] = x[3]/4;                    // dimenzija kruga
             drawlin(g, 5, y, temp);      // STRELICA
             if (!temp.startsWith("NONE")) {
                  y[0] = x[0];
                  y[1] = x[1]-x[6];
                  drawlin(g, 2, y, temp);      // TEKST
             }
             break;
        case 7:  // NIZ POINTERA
             String t1;
             y[0] = x[0];
             y[1] = x[1]-x[3]-x[4];
             y[2] = x[2];
             y[3] = x[3];
             drawlin(g, 1, y, temp);      // PRAVOUGAONIK
             y[0] = x[0]+x[2]/2;               // pocetak x
             y[1] = x[1]-x[3]/2-x[4];          //         y
             y[2] = x[0]+x[2]/2;               // kraj    x
             y[3] = x[1];                      //         y
             y[4] = x[3]/4;                    // dimenzija strelice
             y[5] = 270;                       // ugao
             y[6] = x[3]/4;                    // dimenzija kruga
             drawlin(g, 5, y, temp);      // STRELICA
             y[0] = x[0];
             y[1] = x[1]-x[3]-x[4]-x[6];
             drawlin(g, 2, y, temp);      // TEKST
             for (j = 0; j < 7 ; j++)
                 y[j] = x[j];
             for (j = 0; j < x[7] ; j++) {
                 y[7] = x[8+j];
                 y[1] = x[1]+x[3]*j;
                 drawlin(g, 6, y, "NONE");
             }
             break;
        case 9:  // FONT
             Font f;
             switch(x[0]) {
             case 0:
                 f = new Font(temp, Font.PLAIN, x[1]);
                 g.setFont(f);
                 break;
             case 1:
                 f = new Font(temp, Font.BOLD, x[1]);
                 g.setFont(f);
                 break;
             case 2:
                 f = new Font(temp, Font.ITALIC, x[1]);
                 g.setFont(f);
                 break;
             }
             break;
        case 10:  // BRISE PRAVOUGAONIK
             clearR(g,x[0], x[1], x[2], x[3]);
             break;
        case 11:  // DIMENZIJE CRTEZA
             dx = x[0]*s1/s2;
             dy = x[1]*s1/s2;
             break;
        case 12:  // RAZMERA
             s1 = x[0];
             s2 = x[1];
             break;
        case 13:  // POCETAK PETLJE
             return x[0];
        case 14:  // PAUZA
             return -x[0];
        case 15:  // Komentar
             break;
        }
        return 0;
    }

    void drawL(Graphics g, int x1, int y1, int x2, int y2) {
         g.drawLine(x1*s1/s2, y1*s1/s2, x2*s1/s2, y2*s1/s2);
    }

    void drawR(Graphics g, int x1, int y1, int x2, int y2) {
         g.drawRect(x1*s1/s2, y1*s1/s2, x2*s1/s2, y2*s1/s2);
    }

    void drawS(Graphics g, String s, int x1, int y1) {
         g.drawString(s, x1*s1/s2, y1*s1/s2);
    }

    void clearR(Graphics g, int x1, int y1, int x2, int y2) {
         g.clearRect(x1*s1/s2, y1*s1/s2, x2*s1/s2, y2*s1/s2);
    }

    void fillO(Graphics g, int x1, int y1, int x2, int y2) {
         g.fillOval(x1*s1/s2, y1*s1/s2, x2*s1/s2, y2*s1/s2);
    }

    public Dimension dimenzijeCrteza()  {
        return new Dimension(dx, dy);
    }
    public Dimension razmeraCrteza()  {
        return new Dimension(s1, s2);
    }
}
