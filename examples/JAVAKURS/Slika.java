import java.awt.*;

class Slika extends Canvas {
    Image image;
    Dimension minSize;
    int w, h;
    boolean trueSizeKnown = false;

    Dimension offDimension;
    Image offImage = null;
    Graphics offGraphics = null;

    public Slika(Image image, int initialWidth, int initialHeight) {
	if (image == null) {
	    System.err.println("Canvas got invalid image object!");
	    return;
	}

	this.image = image;
	w = initialWidth;
	h = initialHeight;

	minSize = new Dimension(w,h);
    }

    public Dimension preferredSize() {
 	return minimumSize();
    }

    public synchronized Dimension minimumSize() {
	return minSize;
    }

    public void paint (Graphics g) {
         update(g);
    }


    public void update(Graphics g) {

        //Create the offscreen graphics context, if no good one exists.
        if ( (offGraphics == null)
          || (minSize.width != offDimension.width)
          || (minSize.height != offDimension.height) ) {
            offDimension = minSize;
            offImage = createImage(minSize.width, minSize.height);
            offGraphics = offImage.getGraphics();
        }

	    if (!trueSizeKnown) {
                int imageWidth = offImage.getWidth(this);
                int imageHeight = offImage.getHeight(this);

	        if ((imageWidth > 0) && (imageHeight > 0)) {
		    trueSizeKnown = true;

		    //Component-initiated resizing.
		    w = imageWidth;
		    h = imageHeight;
		    minSize = new Dimension(w,h);
		    resize(w, h);
//                    pappy.validate();
	        }
	    }

        if (image != null) {
            offGraphics.drawImage(image, 0, 0, this);
            g.drawImage(offImage, 0, 0, this);
	}
    }
}
