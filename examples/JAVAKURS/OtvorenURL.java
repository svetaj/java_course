import java.applet.Applet;
import java.io.*;
import java.net.*;

class OtvorenURL 
{
     URL url;

     public OtvorenURL(URL cb, String f) 
     {
          url = null;
          try {
               url = new URL(cb, f);
          } catch (MalformedURLException me) {
               System.out.println("MalformedURLException: " + me);
          } catch (Exception e) {
               System.out.println("Greska: " + e); 
          }
     }

     public URL getOtvorenURL() {
         return url;
     }
}
