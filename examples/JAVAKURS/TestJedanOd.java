import java.awt.*;
import java.util.*;

public class TestJedanOd extends Panel {
    public TestJedanOd(String[] odg) {
        CheckboxGroup cbg;
        Panel cbp = new Panel();
        cbp.setLayout(new GridLayout(0, 1));
        cbg = new CheckboxGroup();
        for (int i = 0; i < odg.length; i++) {
             cbp.add(new Checkbox(odg[i], cbg, false));
        }

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gridbag);
        gbc.insets = new Insets(4,4,5,5);
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;  
        gbc.fill = GridBagConstraints.NONE; 
        gbc.anchor = GridBagConstraints.NORTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda
        gridbag.setConstraints(cbp, gbc);
        add(cbp);
        validate();
    }
}
