import java.awt.*;

class Teorija extends Panel {
    public Teorija(String[] linija) {
        Label lin;
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.weightx = 0.0;                   //reset to the default
        gbc.weighty = 0.0;                   //reset to the default
        gbc.fill = GridBagConstraints.NONE; 
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda

        for(int i=0; i < linija.length; i++) {
              lin = new Label(linija[i]);
              gridbag.setConstraints(lin, gbc);
              add(lin);
        }
    }

    public Teorija(UcitanFajl ucitanFajl) {
        Label lin;
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.weightx = 0.0;                   //reset to the default
        gbc.weighty = 0.0;                   //reset to the default
        gbc.fill = GridBagConstraints.NONE; 
        gbc.gridwidth = GridBagConstraints.REMAINDER; //kraj reda

        ucitanFajl.naPocetakFajla();
        for(int i=0; i < ucitanFajl.brojLinijaFajla(); i++) {
              lin = new Label(ucitanFajl.sledecaLinijaFajla());
              gridbag.setConstraints(lin, gbc);
              add(lin);
        }
    }
}
