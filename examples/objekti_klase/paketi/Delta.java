package Latinski;

import Grcki.*;

class Delta extends Alfa {
    void mtd2(Alfa a, Delta d) {
        a.iZasticeno = 10;    // nije ispravno
        d.iZasticeno = 10;    // ispravno
        a.metodZasticen();    // nije ispravno
        d.metodZasticen();    // ispravno
    }
}
