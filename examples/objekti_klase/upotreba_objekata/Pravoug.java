import java.awt.*;

class Pravoug {
    public static void main(String[] args) {
         Rectangle abc = new Rectangle(0, 0, 100, 200);
         int p = abc.height * abc.width;
         System.out.println("P = " + p);
         abc.x = 25;
         abc.y = 13;
         System.out.println("x=" + abc.x + ", y=" + abc.y);
         if(abc.inside(4,8))
             System.out.println("tacka (4,8) unutar");
         else
             System.out.println("tacka (4,8) van");
         abc.move(3,5);
         System.out.println("x=" + abc.x + ", y=" + abc.y);
         if(abc.inside(4,8))
             System.out.println("tacka (4,8) unutar");
         else
             System.out.println("tacka (4,8) van");
         if (new Rectangle(3,5, 100, 200).equals(abc))
             System.out.println("Jednaki");
         else
             System.out.println("Nisu jednaki");
         if (abc.equals(new Rectangle(0, 0, 50, 70)))
             System.out.println("Jednaki");
         else
             System.out.println("Nisu jednaki");
    }
}
