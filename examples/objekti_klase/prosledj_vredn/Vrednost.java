class Xyz {
    int x, y, z;
}

class Tacka3D {
    int x, y, z;
    public Tacka3D(int xx, int yy, int zz) {
         x = xx; y = yy; z = zz;
    }
    void dajVrednosti(int xx, int yy, int zz) {
         xx = x; yy = y; zz = z;
    }
    void dajVrednosti(Xyz tacka) {
         tacka.x = x; tacka.y = y; tacka.z = z;
    }
}

class Vrednost {
    public static void main(String[] args) {
        int a = -1, b = -1, c = -1;
        Tacka3D inst = new Tacka3D(99, 88, 77);
        Xyz t = new Xyz();

        inst.dajVrednosti(a, b, c);
        System.out.println("x = " + a + ", y = " + b + ", z = " + c);

        inst.dajVrednosti(t);
        System.out.println("x = " + t.x + ", y = " + t.y + ", z = " + t.z);
    }
}
