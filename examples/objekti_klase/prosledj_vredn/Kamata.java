class Kamata {
	public static void main(String[] args) {
                double rata, k = 10, g = 1000.0;
                int b = 12;                     
                Rata r = new Rata();
		System.out.println("glavnica = " + g);
		System.out.println("broj rata = " + b);
		System.out.println("kamata = " + k);
                System.out.println("rata = " + r.izracunajRatu(g, k, 0., b));
	}
}

class Rata {
	double izracunajRatu(double glavnica, double kamata, 
     	                 double novaGlavnica, int brojRata) {
	    double I, deo, brojilac, odgovor;

	    I = kamata / 100.0;
	    deo = Math.pow((1 + I), (0.0 - brojRata));
	    brojilac = (1 - deo) / I;
	    odgovor = ((-1 * glavnica) / brojilac)
	             - ((novaGlavnica * deo) / brojilac);
	    return odgovor;
	}
}
