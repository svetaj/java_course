// Demonstracija metoda koji prikazuje ime klase
// Promeniti metod u static .....

class DajIme {
	public static void main(String[] args) {
		Integer abc = new Integer(1);
		String pqr = new String("trt");
        Ime x = new Ime();
        x.dajImeKlase(abc);
        x.dajImeKlase(pqr);
        x.dajImeKlase(x);
	}
}

class Ime {
        void dajImeKlase(Object obj) {
	    System.out.println("Klasa " + obj.getClass().getName());
	}
}
