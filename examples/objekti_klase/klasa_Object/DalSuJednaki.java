class DalSuJednaki {
	public static void main(String[] args) {
		Integer abc = new Integer(1), xyz = new Integer(1);
		if (abc.equals(xyz))
			System.out.println("objekti abc i xyz jesu jednaki");

		String pqr = new String("trt"), mnp = new String("mrt");
		if (pqr.equals(mnp))
			System.out.println("objekti pqr i mnp jesu jednaki");
	}
}
