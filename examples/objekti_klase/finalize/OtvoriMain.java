import java.io.*;

class OtvoriFajl {
    FileInputStream fajlStrim = null;
    OtvoriFajl(String imefajla) {
        try {
             fajlStrim = new FileInputStream(imefajla);
        } catch (java.io.FileNotFoundException e) {
             System.err.println("Ne mogu da otvorim fajl " + imefajla);
        }
    }
    protected void finalize () throws Throwable {
        System.out.println("finalize metod");
        if (fajlStrim != null) {
             fajlStrim.close();
             fajlStrim = null;
        }
    }
}

class Xyz {
    void abc() {
         OtvoriFajl abc = new OtvoriFajl("xyz.pqr");
    }
    void pqr() {
         System.out.println("u metodu pqr");
    }
}

class OtvoriMain {
    public static void main(String[] args) {
         Xyz x = new Xyz();
         x.abc();
         x.pqr();
         System.gc();
    }
}
