package Dva;
import Jedan.*;

public class AlfaDva extends Alfa {
    public static void main(String[] args) {
        Alfa a = new Alfa();
        //a.privateMetod();   //neispravno
        //a.packageMetod();   //neispravno
        //a.protectedMetod(); //neispravno
        a.publicMetod();       //ispravno

        //System.out.println("jasamprivate: "
        //   + a.jasamprivate);    //neispravno
        //System.out.println("jasampackage: "
        //   + a.jasampackage);    //neispravno
        //System.out.println("jasamprotected: "
        //   + a.jasamprotected);  //neispravno
        System.out.println("jasampublic "
            + a.jasampublic);      //ispravno

        AlfaDva a2 = new AlfaDva();
        a2.protectedMetod();   //ispravno
        System.out.println("jasamprotected: "
            + a2.jasamprotected); //ispravno
    }
}

