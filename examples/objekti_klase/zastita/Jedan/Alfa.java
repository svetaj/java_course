package Jedan;
public class Alfa {
    //promenljive clanice
    private   int jasamprivate = 1;
              int jasampackage = 2;  //package access
    protected int jasamprotected = 3;
    public    int jasampublic = 4;

    //Metodi
    private void privateMetod() {
        System.out.println("jasamprivate Metod");
    }
    void packageMetod() { //package access
        System.out.println("jasampackage Metod");
    }
    protected void protectedMetod() {
        System.out.println("jasamprotected Metod");
    }
    public void publicMetod() {
        System.out.println("jasampublic Metod");
    }

    public static void main(String[] args) {
        Alfa a = new Alfa();
        a.privateMetod();   //ispravno
        a.packageMetod();   //ispravno
        a.protectedMetod(); //ispravno
        a.publicMetod();    //ispravno

        System.out.println("jasamprivate: "
            + a.jasamprivate);    //ispravno
        System.out.println("jasampackage: "
            + a.jasampackage);    //ispravno
        System.out.println("jasamprotected: "
            + a.jasamprotected); //ispravno
        System.out.println("jasampublic: "
            + a.jasampublic);     //ispravno
    }
}

