package Jedan;
public class DeltaJedan {
    public static void main(String[] args) {
        Alfa a = new Alfa();
        a.privateMetod();  //nespravno
        a.packageMetod();    //ispravno
        a.protectedMetod();  //ispravno
        a.publicMetod();     //ispravno
        System.out.println("jasamprivate: "
          + a.jasamprivate);   //neispravno
        System.out.println("jasampackage: "
            + a.jasampackage);   //ispravno
        System.out.println("jasamprotected: "
            + a.jasamprotected); //ispravno
        System.out.println("jasampublic: "
            + a.jasampublic);    //ispravno
    }
}

