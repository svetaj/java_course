// privatni podaci i pristup iz iste klase

class Alfa {
    private int iPrivatno;
    private void metodPrivatni() {
        System.out.println("privatni metod");
    }
    boolean jednakJeSa(Alfa drugaAlfa) {
        if (this.iPrivatno == drugaAlfa.iPrivatno)
            return true;
        else
            return false;
    }
}

class Beta {
    void mtd1() {
        Alfa a = new Alfa();
        a.iPrivatno = 10;      // nije dozvoljeno
        a.metodPrivatni();     // nije dozvoljeno
    }
}
