class CeoBrojX {
    static int x;              // i ovde mora static !!
    static public int x() {
        return x;
    }
    static public void postaviX(int novoX) {
        x = novoX;
    }
}

class DemoX {
    public static void main(String[] args) {
         CeoBrojX.postaviX(1);         
         System.out.println("CeoBrojX.x = " + CeoBrojX.x());
    }
}
