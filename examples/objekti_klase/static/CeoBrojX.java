class CeoBrojX {
    int x;
    public int x() {
        return x;
    }
    public void postaviX(int novoX) {
        x = novoX;
    }
}

class DemoX {
    public static void main(String[] args) {
         CeoBrojX mojeX = new CeoBrojX();
         CeoBrojX drugoX = new CeoBrojX();
         mojeX.postaviX(1);                      // ili mojeX.x = 1;
         drugoX.x = 2;
         System.out.println("mojeX.x = " + mojeX.x());
         System.out.println("drugoX.x = " + drugoX.x());
    }
}
