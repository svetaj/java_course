// demonstrira upotrebu super

class SuperAbc {
    boolean a;
    void metod() {
        a = true;
    }
}

class Abc extends SuperAbc {
    boolean a;
    void metod() {
        a = false;
        super.metod();
        System.out.println(a);
        System.out.println(super.a);
    }
}

class MainAbc {
    public static void main(String[] args) {
        Abc x = new Abc();
        x.metod();
    }
}
