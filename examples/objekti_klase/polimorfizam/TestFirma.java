class TestFirma {
	public static void main(String[] args)
	{
		System.out.println("PRIMER NASLEDJIVANJA");
		System.out.println("====================\n\n");

        Nadnicar pera = new Nadnicar("Pera Peric");
        pera.odrediSate (40.0f);
        pera.odrediNadnicu (5.0f);
        System.out.println(pera.funkcijaIme());
        System.out.println( "Plata nadnicara: " + (int) pera.obracunPlate() );

        Trgovac mika = new Trgovac("Mika Mikic");
        mika.odrediSate (40.0f);
        mika.odrediNadnicu (6.0f);
        mika.odrediProviziju (0.05f);
        mika.odrediProdaju (2000.0f);
        System.out.println(mika.funkcijaIme());
        System.out.println( "Plata prodavca: " + (int) mika.obracunPlate() );

        Direktor laza = new Direktor("Laza Lazic");
        laza.odrediPlatu (100000.0f);
        System.out.println(laza.funkcijaIme());
        System.out.println( "Plata direktora: " + (int) laza.obracunPlate() );

		System.out.println("\n\n\nPOLIMORFIZAM");
		System.out.println("====================\n\n");

		Radnik[] firma = new Radnik[3];
		firma[0] = pera;
		firma[1] = mika;
		firma[2] = laza;
		for (int i = 0; i < firma.length; i++)
		{
			 System.out.println(Radnik.podaci(firma[i]));
			 System.out.println( "Plata: " + (int) Radnik.plata(firma[i]) );
		}

	}
}


