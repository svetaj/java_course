class Trgovac extends Nadnicar
{
	private final String funkcija = "TRGOVAC";
	private float provizija;
    private float prodato;

	public Trgovac (String ime)
	{
		super (ime);
        provizija = 0.0f;
        prodato = 0.0f;
	}

    public void odrediProviziju (float provizija)
    {
		this.provizija = provizija;
	}

    public void odrediProdaju (float prodato)
    {
		this.prodato = prodato;
	}

    public float obracunPlate()
    {
		return super.obracunPlate() + provizija * prodato;
	}

	public String funkcijaIme()
	{
		return funkcija + ": " + dajIme();
	}

}

