/* Primer nasledjivanja klasa */

#include <iostream.h>
#include <string.h>

class Radnik
{
public:
        Radnik();
        Radnik (const char *im);
        char *dajIme() const { return (char *)ime; }  
private:
        char ime[30];
};
              
Radnik::Radnik( const char *im)
{
        strcpy(ime, im);
}

class Nadnicar : public Radnik
{
public:
        Nadnicar (const char *im);
        void odrediNadnicu (float nad) {nadnica = nad;}
        void odrediSate (float s) {sati = s;} 
        void prikaziIme() const; 
        float obracunPlate() const { return nadnica * sati;}
private:
        float nadnica;
        float sati;
};

Nadnicar::Nadnicar (const char *im)
        :Radnik (im)
{
        nadnica = 0.0;
        sati = 0.0;
}	

void Nadnicar::prikaziIme() const
{
        cout << "Ime radnika: " << dajIme() << '\n'; // Call Radnik::dajIme 
}
 
class Trgovac : public Nadnicar
{
public:
        Trgovac (const char *im);
        void odrediProviziju (float prov) {provizija = prov;}
        void odrediProdaju (float prod) {prodato = prod;}     
        float Trgovac::obracunPlate() const 
        { return Nadnicar::obracunPlate() + provizija * prodato;}
private:
        float provizija;
        float prodato;
};

Trgovac::Trgovac (const char *im)
        :Nadnicar (im)
{               
        provizija = 0.0;
        prodato = 0.0;
}	

class Direktor : public Radnik
{
public:
        Direktor (const char *im);
        void odrediPlatu (float pl) {plata = pl;}
        float Direktor::obracunPlate() const { return plata;}
private:
        float plata;
};

Direktor::Direktor (const char *im)
        : Radnik (im)
{                   
        plata = 0.0;
}



main()
{
        Nadnicar pera ("Pera Peric");
	char *str;
	
        pera.odrediSate (40.0);                // zove Nadnicar::odrediSate()
        str = pera.dajIme();                   // zove Radnik::dajIme() 
        cout << "Ime radnika = " << str << '\n';   
        pera.prikaziIme();
	
        Trgovac mika ("Mika Mikic");
	
        mika.odrediSate (40.0);
        mika.odrediNadnicu (6.0);
        mika.odrediProviziju (0.05);
        mika.odrediProdaju (2000.0);
        mika.prikaziIme();
	
        // zove Trgovac::obracunPlate
        cout << "Plata prodavca: " << (int) mika.obracunPlate() << '\n'; 
        cout << "Osnovna plata prodavca: "
             <<  (int) mika.Nadnicar::obracunPlate() << '\n';
	
	return 0;	
}
 
 
