class Direktor extends Radnik
{
	private final String funkcija = "DIREKTOR";
	private float plata;

	public Direktor (String ime) {
		super (ime);
        plata = 0.0f;
	}

    public void odrediPlatu (float plata)
    {
		this.plata = plata;
	}

    public float obracunPlate()
    {
		return plata;
	}

	public String funkcijaIme()
	{
		return funkcija + ": " + dajIme();
	}

}


