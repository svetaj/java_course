class Nadnicar extends Radnik
{
	private final String funkcija = "NADNICAR";
	private float nadnica;
    private float sati;

	public Nadnicar (String ime) {
		super (ime);
        nadnica = 0.0f;
        sati = 0.0f;
	}

	public Nadnicar () {
		super (" ");
        nadnica = 0.0f;
        sati = 0.0f;
	}

	public void odrediNadnicu (float nadnica)
	{
		this.nadnica = nadnica;
	}

	public void odrediSate (float sati)
	{
		this.sati = sati;
	}

	public String funkcijaIme()
	{
		return funkcija + ": " + dajIme();
	}

	public float obracunPlate()
	{
		return nadnica * sati;
	}
}
