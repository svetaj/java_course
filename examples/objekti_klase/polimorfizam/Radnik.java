abstract class Radnik
{
	private final String ime;
	private final String funkcija = "RADNIK";

    public Radnik (String ime)
    {
		this.ime = ime;
	}

    public String dajIme()
    {
		return ime;
	}

    abstract public float obracunPlate();
    abstract public String funkcijaIme();

    // POLIMORFIZAM gde se vrsi tzv, "UPCASTING"

    public static float plata(Radnik r)
    {
		return r.obracunPlate();
	}

	public static String podaci(Radnik r)
	{
		return r.funkcijaIme();
	}

}
