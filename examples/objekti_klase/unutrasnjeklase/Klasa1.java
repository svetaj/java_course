public class Klasa1 {
    protected UnutrasnjaKlasa1 ic;

    public Klasa1() {
		ic = new UnutrasnjaKlasa1();
    }

    public void prikaziString() {
		System.out.println(ic.uzmiString() + ".");
		System.out.println(ic.uzmiDrugiString() + ".");
    }

    static public void main(String[] args) {
        Klasa1 c1 = new Klasa1();
		c1.prikaziString();
    }

    protected class UnutrasnjaKlasa1 {
		public String uzmiString() {
	    	return "UnutrasnjaKlasa1: uzmiString pozvano";
		}

		public String uzmiDrugiString() {
	    	return "UnutrasnjaKlasa1: uzmiDrugiString pozvano";
		}
    }
}
