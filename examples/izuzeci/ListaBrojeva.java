import java.io.*;
import java.util.Vector;

public class ListaBrojeva {
    private Vector niz;
    private static final int DIMENZ = 10;

    public ListaBrojeva () {
        niz = new Vector(DIMENZ);
        for (int i = 0; i < DIMENZ; i++)
            niz.addElement(new Integer(i));
    }
    public void writeList() {
        PrintWriter out = null;

        try {
            System.out.println("Ulazi u try iskaz");
            out = new PrintWriter(new FileWriter("IzlFajl.txt"));

            for (int i = 0; i < DIMENZ; i++)
                out.println("Vrednost: " + i + " = " + niz.elementAt(i));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Uvaceno ArrayIndexOutOfBoundsException: " +
                                 e.getMessage());
        } catch (IOException e) {
            System.err.println("Uvaceno IOException: " + e.getMessage());
        } finally {
            if (out != null) {
                System.out.println("Zatvara PrintWriter");
                out.close();
            } else {
                System.out.println("PrintWriter nije otvoren");
            }
        }
    }
}
