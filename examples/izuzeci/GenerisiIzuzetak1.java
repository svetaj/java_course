
class GenerisiIzuzetak1  {
	public void izuzetak () throws NullPointerException {
		Object t = null;
		if(t == null)
		  throw new NullPointerException("Upomoc, greska, t = null");

	}
}

class PrihvatiIzuzetak {
	public static void main(String[] args) {
		try {
			new GenerisiIzuzetak1().izuzetak();
		}
		catch (NullPointerException e) {
			System.err.println("Uvacen izuzetak: " + e.getMessage());
		}
	}
}