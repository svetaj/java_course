public class ToStringDemo {
    public static void main(String[] args) {
        double d = 858.48;
        String s = Double.toString(d);

        int dot = s.indexOf('.');
        System.out.println(s.substring(0, dot).length()
                           + " cifara pre decimalne tacke.");
        System.out.println(s.substring(dot+1).length()
                           + " cifara posle decimalne tacke.");
    }
}
