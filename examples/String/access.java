class Filename {
    String fullPath;
    char pathSeparator;

    Filename(String str, char sep) {
        fullPath = str;
        pathSeparator = sep;
    }

    String extension() {
        int dot = fullPath.lastIndexOf('.');
        return fullPath.substring(dot + 1);
    }

    String filename() {
        int dot = fullPath.lastIndexOf('.');
        int sep = fullPath.lastIndexOf(pathSeparator);
        return fullPath.substring(sep + 1, dot);
    }

    String path() {
        int sep = fullPath.lastIndexOf(pathSeparator);
        return fullPath.substring(0, sep);
    }
}

class FilenameTest {
    public static void main(String[] args) {
        Filename myHomePage = new Filename("/home/mem/public_html/index.html", '/');
        System.out.println("Extension = " + myHomePage.extension());
        System.out.println("Filename = " + myHomePage.filename());
        System.out.println("Path = " + myHomePage.path());
    }
}