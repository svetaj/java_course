public class CharacterDemo {
    public static void main(String args[]) {
        Character a = new Character('a');
        Character a2 = new Character('a');
        Character b = new Character('b');

        int razlika = a.compareTo(b);

	if (razlika == 0) {
            System.out.println("a jednako b.");
        } else if (razlika < 0) {
            System.out.println("a manje od b.");
        } else if (razlika > 0) {
            System.out.println("a vece od b b.");
        }

        System.out.println("a " + ((a.equals(a2)) ? "je jednako" : "nije jednako")
                           + " sa a2.");

	System.out.println("Znak " + a.toString() + " je "
                   + (Character.isUpperCase(a.charValue()) ? "veliko" : "malo")
                   + "slovo.");
    }
}
