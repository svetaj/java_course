public class ValueOfDemo {
    public static void main(String[] args) {

	//program zahteva dva argumenta u komandnoj liniji
        if (args.length == 2) {

            //knvertuje stringove u brojeve
            float a = Float.valueOf(args[0]).floatValue();
            float b = Float.valueOf(args[1]).floatValue();

            //malo aritmetike
            System.out.println("a + b = " + (a + b) );
            System.out.println("a - b = " + (a - b) );
            System.out.println("a * b = " + (a * b) );
            System.out.println("a / b = " + (a / b) );
            System.out.println("a % b = " + (a % b) );
        } else {
            System.out.println("program zahteva dva argumenta u komandnoj liniji.");
        }
    }
}
