// Zadatak za vezbu klasa String, StringBuffer i StringTokenizer
// (C) Sveta 2006
// 1. Doraditi program komandu POREDI
// 2. Doraditi program komandu TRAZI
// 3. Uprostiti program pomocu upotrebe private metoda
// 4. Dozvoliti da interpreter prihvata komande duzine do 4 znaka, bez
//    obzira na mala i velika slova
// 5. Zastititi program od kontrolnih znakova <Ctrl-C> itd.


import java.io.*;
import java.util.*;

class StringDemo {
    public static void main(String[] args)
//        throws java.io.IOException
    {
		try {
        	BufferedReader in = new BufferedReader(
				new InputStreamReader(System.in));
        	String linija = "";
        	String komanda = null;
        	String[] argx = new String[6];
        	StringTokenizer st = null;
        	while (true) {
            	System.out.print("> prompt ");
            	if ( (linija = in.readLine()) == null) continue;
            	System.out.println("KOMANDNA LINIJA:"+linija);
            	st = new StringTokenizer(linija);
				if (! st.hasMoreTokens() ) continue;
				komanda = st.nextToken();
            	System.out.println("KOMANDA:"+komanda);
            	if (komanda.equals("POMOC")) {
					System.out.println("----------------------------");
					System.out.println("POMOC   - prikazuje pomoc");
					System.out.println("BROJ    - broji sve znakove");
					System.out.println("VELIKA  - u velika slova");
					System.out.println("PODNIZ  - prikazuje podniz");
					System.out.println("POREDI  - poredi nizove");
					System.out.println("TRAZI   - trazi niz");
					System.out.println("IZLAZ   - izlaz iz programa");
					System.out.println("----------------------------");
			    }
            	else if (komanda.equals("BROJ")) {
					if (st.countTokens() != 1) continue;
					System.out.println("Duzina stringa je " + st.nextToken().length());
				}
            	else if (komanda.equals("VELIKA")) {
					if (st.countTokens() != 1) continue;
					System.out.println("Novi string je " + st.nextToken().toUpperCase());
				}
            	else if (komanda.equals("PODNIZ")) {
					if (st.countTokens() != 3) continue;
					argx[0] = st.nextToken();
					argx[1] = st.nextToken();
					argx[2] = st.nextToken();
					int i1 = new Integer (argx[1]).intValue();
					int i2 = new Integer (argx[2]).intValue();
					System.out.println("Novi string je " + argx[0].substring(i1,i2));
				}
            	else if (komanda.equals("POREDI")) {
					System.out.println("Uraditi za vezbu!");
					// pozvati iz String metod equals(String)
					// dodatak: smatrati da su jednaki bez obzira
					// na mala i velika slova, ako je jednako prvih
					// 10 znakova. koristiti toUpperCase i substring
					continue;
				}
            	else if (komanda.equals("TRAZI")) {
					System.out.println("Uraditi za vezbu!");
					// pozvati iz String metod indexOf(String)
					continue;
				}
            	else if (komanda.equals("IZLAZ")) {
					System.out.println("Kraj programa!");
					break;
				}
        	}
    	} catch (IOException e) {
 					System.out.println("IO Exception!");
   		}
	}
}
