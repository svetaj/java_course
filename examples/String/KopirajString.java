class KopirajString {
    public static void main(String[] args) {
        String s = "Danas je suncan dan!";
        StringBuffer t = new StringBuffer("Sneg je i hladno je");

        System.out.println(s + ", " + t);
        mojStrCopy(t, s);
        System.out.println(s + ", " + t);
    }

    static void mojStrCopy(StringBuffer dest, String src) {
        int i, len = src.length();

        for (i = 0; i < len; i++)
            dest.setCharAt(i, src.charAt(i));
    }
}