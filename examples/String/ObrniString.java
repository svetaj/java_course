class ObrniString {
    public static void main(String[] args) {
        String abc = new String("Dobar dan");
        String xyz = new String(obrniGu(abc));
        System.out.println(abc + " <--> " + xyz);
    }

    public static String obrniGu(String izvor) {
        int i, duz = izvor.length();
        StringBuffer odrediste = new StringBuffer(duz);
// Od SDK 5.0 koristi se StringBuiler umesto StringBuffer
//        StringBuilder odrediste = new StringBuilder(duz);

        for (i = (duz - 1); i >= 0; i--) {
            odrediste.append(izvor.charAt(i));
        }
        return odrediste.toString();
    }
}
