import java.io.FileInputStream;
import java.util.Properties;

class PropertiesTest2 {
    public static void main(String[] args) {
        try {
                // postavlja nove properties iz fajla "moje.txt"
            FileInputStream propFajl = new FileInputStream("moje.txt");
            Properties p = new Properties(System.getProperties());
            p.load(propFajl);

                // postavlja sistemske osobine
            System.setProperties(p);
            System.getProperties().list(System.out);    // postavlja nove osobine
        } catch (java.io.FileNotFoundException e) {
            System.err.println("Ne mogu da nadjem moje.txt.");
        } catch (java.io.IOException e) {
            System.err.println("U/I neuspesno.");
        }
    }
}