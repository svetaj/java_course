/**
 * ContinueDemo.java
 */
public class ContinueDemo {
    public static void main(String[] args) {

        StringBuffer pretraziMe = new StringBuffer(
                  "peter piper picked a peck of pickled peppers");
        int max = pretraziMe.length();
        int numPs = 0;

	//NAPOMENA: Ne mozemo da koristimo foreach jer StringBuffer
	//nije Collection.
        for (int i = 0; i < max; i++) {
	    //samo nas p zanima
            if (pretraziMe.charAt(i) != 'p')
	        continue;

	    //obradi p
	    numPs++;
            pretraziMe.setCharAt(i, 'P');
        }
        System.out.println("Nadjeno " + numPs + " pojavljivanja p u nizu");
        System.out.println(pretraziMe);
    }
}
