/**
 * BreakDemo.java
 */

public class BreakDemo {
    public static void main(String[] args) {

        int[] nizCelih = { 32, 87, 3, 589, 12, 1076,
                              2000, 8, 622, 127 };
        int tragaseza = 12;

        int i = 0;
        boolean nadjeno = false;

	//Ne koristimo for-each jer nam treba vrednost i
        for ( ; i < nizCelih.length; i++) {
            if (nizCelih[i] == tragaseza) {
                nadjeno = true;
                break;
	    }
        }

        if (nadjeno) {
	    System.out.println("Nadjeno " + tragaseza + " na indeksu " + i);
        } else {
	    System.out.println(tragaseza + "nije u nizu");
        }
    }
}
