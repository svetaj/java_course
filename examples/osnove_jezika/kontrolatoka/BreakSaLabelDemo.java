/**
 * BreakSaLabelDemo.java
 */

public class BreakSaLabelDemo {
    public static void main(String[] args) {

        int[][] nizCelih = { { 32, 87, 3, 589 },
                                { 12, 1076, 2000, 8 },
                                { 622, 127, 77, 955 }
                              };
        int tragaseza = 12;

        int i = 0;
        int j = 0;
        boolean nadjeno = false;

    trazi:
	// Ne koristimo for-each jer nam treba i & j.
        for ( ; i < nizCelih.length; i++) {
            for (j = 0; j < nizCelih[i].length; j++) {
                if (nizCelih[i][j] == tragaseza) {
                    nadjeno = true;
                    break trazi;
	        }
            }
        }

        if (nadjeno) {
	    System.out.println("Nadjeno " + tragaseza + " na " + i + ", " + j);
        } else {
            System.out.println(tragaseza + "nije u nizu");
        }

    }
}
