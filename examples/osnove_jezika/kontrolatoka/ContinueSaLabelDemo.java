/**
 * ContinueWithLabelDemo.java
 * U pravom programu koristiti indexof(String, int).
 */

public class ContinueSaLabelDemo {
    public static void main(String[] args) {

        String pretraziMe = "Look for a podniz in me";
        String podniz = "sub";
        boolean nadjeno = false;

        int max = pretraziMe.length() - podniz.length();

    test:
        for (int i = 0; i <= max; i++) {
            int n = podniz.length();
            int j = i;
            int k = 0;
            while (n-- != 0) {
                if (pretraziMe.charAt(j++) != podniz.charAt(k++)) {
                    continue test;
                }
            }
            nadjeno = true;
            break test;
        }
        System.out.println(nadjeno ? "Nadjeno" : "Nije nadjeno");
    }
}
