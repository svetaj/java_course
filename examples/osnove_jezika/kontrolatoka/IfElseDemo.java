public class IfElseDemo {
    public static void main(String[] args) {

        int brojpoena = 76;
        char ocena;

        if (brojpoena >= 90) {
            ocena = 'A';
        } else if (brojpoena >= 80) {
            ocena = 'B';
        } else if (brojpoena >= 70) {
            ocena = 'C';
        } else if (brojpoena >= 60) {
            ocena = 'D';
        } else {
            ocena = 'F';
        }
        System.out.println("ocena = " + ocena);
    }
}
