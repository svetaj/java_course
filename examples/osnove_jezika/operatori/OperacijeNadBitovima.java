public class OperacijeNadBitovima {

    static final int RUCNA = 1;
    static final int MOTOR = 2;
    static final int KVACILO = 4;
    static final int GAS = 8;

    public static void main(String[] args) {
        int indik = 0;

        indik = indik | RUCNA;
        indik = indik | MOTOR;

        if ((indik & RUCNA) == RUCNA) {
            if ((indik & MOTOR) == MOTOR) {
                 System.out.println("Indikatori RUCNA "
                                    + "i MOTOR su ukljuceni.");
            }
        }

        indik = indik | GAS;

        if ((indik & GAS) == GAS) {
           System.out.println("Indikator GAS je ukljucen.");
        }
    }
}
