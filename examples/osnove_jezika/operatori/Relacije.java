public class Relacije {
    public static void main(String[] args) {

        //nekoliko brojeva
        int i = 37;
        int j = 42;
        int k = 42;
        System.out.println("Vrednosti promenljivih...");
        System.out.println("    i = " + i);
        System.out.println("    j = " + j);
        System.out.println("    k = " + k);

	//vece od
        System.out.println("Vece od...");
        System.out.println("    i > j jeste " + (i > j));     //false
        System.out.println("    j > i jeste " + (j > i));     //true
        System.out.println("    k > j jeste " + (k > j));     //false, jednaki su

	//vece ili jednako
        System.out.println("vece ili jednako...");
        System.out.println("    i >= j jeste " + (i >= j));   //false
        System.out.println("    j >= i jeste " + (j >= i));   //true
        System.out.println("    k >= j jeste " + (k >= j));   //true

	//manje od
        System.out.println("manje od...");
        System.out.println("    i < j jeste " + (i < j));     //true
        System.out.println("    j < i jeste " + (j < i));     //false
        System.out.println("    k < j jeste " + (k < j));     //false

	//manje ili jednako od
        System.out.println("manje ili jednako...");
        System.out.println("    i <= j jeste " + (i <= j));   //true
        System.out.println("    j <= i jeste " + (j <= i));   //false
        System.out.println("    k <= j jeste " + (k <= j));   //true

	//jednako
        System.out.println("jednako...");
        System.out.println("    i == j jeste " + (i == j));   //false
        System.out.println("    k == j jeste " + (k == j));   //true

	//nije jednako
        System.out.println("nije jednako...");
        System.out.println("    i != j jeste " + (i != j));   //true
        System.out.println("    k != j jeste " + (k != j));   //false

    }
}
