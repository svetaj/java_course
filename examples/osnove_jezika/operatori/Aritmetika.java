/**
 * Aritmetika.java
 */

public class Aritmetika {
    public static void main(String[] args) {

        //nekoliko brojeva
        int i = 37;
        int j = 42;
        double x = 27.475;
        double y = 7.22;
        System.out.println("Vrednosti promenljivih...");
        System.out.println("    i = " + i);
        System.out.println("    j = " + j);
        System.out.println("    x = " + x);
        System.out.println("    y = " + y);

        //sabiranje
        System.out.println("Sabiranje...");
        System.out.println("    i + j = " + (i + j));
        System.out.println("    x + y = " + (x + y));

        //oduzimanje
        System.out.println("Oduzimanje...");
        System.out.println("    i - j = " + (i - j));
        System.out.println("    x - y = " + (x - y));

        //mnozenje
        System.out.println("Mnozenje...");
        System.out.println("    i * j = " + (i * j));
        System.out.println("    x * y = " + (x * y));

        //deljenje
        System.out.println("Deljenje...");
        System.out.println("    i / j = " + (i / j));
        System.out.println("    x / y = " + (x / y));

        //racunanje ostatka
        System.out.println("Racunanje ostatka...");
        System.out.println("    i % j = " + (i % j));
        System.out.println("    x % y = " + (x % y));

        //mesanje tipova
        System.out.println("Mesanje tipova...");
        System.out.println("    j + y = " + (j + y));
        System.out.println("    i * x = " + (i * x));
    }
}
