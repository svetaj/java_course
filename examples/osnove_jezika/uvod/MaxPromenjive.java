/**
 * MaxPromenjive.java
 */

public class MaxPromenjive {
    public static void main(String args[]) {

        // celobrojne
        byte largestByte = Byte.MAX_VALUE;
        short largestShort = Short.MAX_VALUE;
        int largestInteger = Integer.MAX_VALUE;
        long largestLong = Long.MAX_VALUE;

        // realni brojevi
        float largestFloat = Float.MAX_VALUE;
        double largestDouble = Double.MAX_VALUE;

        // drugi primitivni tipovi
        char aChar = 'S';
        boolean aBoolean = true;

        // prikaz svih njih
        System.out.println("Najveca byte vrednost je " + largestByte);
        System.out.println("Najveca short vrednost je " + largestShort);
        System.out.println("Najveca integer vrednost je " + largestInteger);
        System.out.println("Najveca long vrednost je " + largestLong);

        System.out.println("Najveca float vrednost je " + largestFloat);
        System.out.println("Najveca double vrednost je " + largestDouble);

        if (Character.isUpperCase(aChar)) {
            System.out.println("Znak " + aChar + " je veliko slovo.");
        } else {
            System.out.println("Znak " + aChar + " je malo slovo.");
        }
        System.out.println("Bulova vrednost je " + aBoolean);
    }
}
