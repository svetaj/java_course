#include <stdio.h>
struct radnik {
    char ime[DUZIMENA];
    char adresa[DUZADRESE];
    long reg_broj;
    double plata;
    double (*povisica)(double, double);
};
double povisica(double plata, double procenat)
{
    return plata * procenat;
}
main()
{
    struct radnik guja = {
        "Crna Guja",
        "Uljica od jedna ptica",
        123456789,
        45000.00,
        povisica
    };
    printf("povisica = %f\n", guja.povisica(guja.plata, 0.10));
}
