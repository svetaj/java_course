class Radnik {
    String ime;
    String adresa;
    long reg_broj;
    private double plata;
    double povisica(double procenat) {
        return procenat * plata;
    }
    Radnik(String ime1, String adresa1, long reg_broj1, double plata1){
        ime = ime1;
        adresa = adresa1;
        reg_broj = reg_broj1;
        plata = plata1;
    };
}
class GlavnaKlasa {
    public static void main(String[] args) {
        Radnik guja = new Radnik("Crna Guja", "Uljica od jedna ptica",
                          123456789, 45000.0);
        System.out.println("povisica = " + guja.povisica(0.10));
    }
}
