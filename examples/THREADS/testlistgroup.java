class TestListGroup {
    public static void main (String[] args) {
        new SimpleThread("Jamaica").start();
        new SimpleThread("Fiji").start();
        new SimpleThread("Bora Bora").start();
	EnumerateTest.listCurrentThreads();
    }
}

class SimpleThread extends Thread {
    public SimpleThread(String str) {
        super(str);
    }
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(i + " " + getName());
            try {
                sleep((int)(Math.random() * 1000));
            } catch (InterruptedException e) {}
        }
        System.out.println("DONE! " + getName());
    }
}

class EnumerateTest {
    static void listCurrentThreads() {
        ThreadGroup currentGroup = Thread.currentThread().getThreadGroup();
        int numThreads;
        Thread[] listOfThreads;

        numThreads = currentGroup.activeCount();
        listOfThreads = new Thread[numThreads];
        currentGroup.enumerate(listOfThreads);
        for (int i = 0; i < numThreads; i++) {
            System.out.println("Thread #" + i + " = " + listOfThreads[i].getName());
        }
    }
}
