<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<title>Thread Scheduling</title>
<script language="JavaScript">
<!-- hide
function openWin(term) {
  url="../../information/glossary.html#" + term;
  myWin= window.open(url, "Glossary",
              "width=400,height=150,scrollbars=yes,status=no,toolbar=no,menubar=no");
  myWin.focus();
}
//-->
</script>
<STYLE type="text/css">
<!--
.FigureCaption   { margin-left: 1in; margin-right: 1in; font-family: Arial, Helvetica, sans-serif; font-size: smaller; text-align: justify }
-->
</STYLE> 
</head>
<body BGCOLOR="#ffffff" LINK="#000099">
<B><FONT SIZE="-1">The Java</font><sup><font size="-2">TM</font></sup> <font size="-1">Tutorial</FONT></B>
<br>
<table width="550" summary="layout">
<tr>
<td align="left" valign="middle">
<a href="lifecycle.html" target="_top"><img src="../../images/PreviousArrow.gif" width="26" height="26" align="middle" border="0" alt="Previous Page"></a>
<a href="../TOC.html#threads" target="_top"><img src="../../images/TOCIcon.gif" width="26" height="26" align="middle" border="0" alt="Lesson Contents"></a>
<a href="multithreaded.html" target="_top"><img src="../../images/NextArrow.gif" width="26" height="26" align="middle" border="0" alt="Next Page"></a></td>

<td align="center" valign="middle">
<font size="-1">
<a href="../../index.html" target="_top">Start of Tutorial</a>
&gt;
<a href="../index.html" target="_top">Start of Trail</a>
&gt;
<a href="index.html" target="_top">Start of Lesson</a>
</font>
</td>

<td align="right" valign="middle">
<font size="-1">
<a href="../../search.html" target="_top">Search</a>
<br><a href="../../forms/sendusmail.html" target="_blank">Feedback Form</a>
</font>
</td>

</tr>
</table>
<img src="../../images/blueline.gif" width="550" height="8" ALIGN="BOTTOM" ALT="">
<br>

<font size="-1">
<b>Trail</b>: Essential Java Classes
<br>
<b>Lesson</b>: Threads: Doing Two or More Tasks At Once
</font>

<h2>
Thread Scheduling
</h2>
<blockquote>
As mentioned briefly in the previous section, many computer 
configurations have a single CPU. Hence, threads run one at 
a time in such a way as to provide an illusion of concurrency. 
Execution of multiple threads on a single CPU in some order is 
called <I>scheduling</I>. The Java runtime environment supports 
a very simple, deterministic scheduling algorithm called 
<I>fixed-priority scheduling</I>. This algorithm schedules 
threads on the basis of their priority relative to other 
Runnable threads. 
<P>
When a thread is created, it inherits its priority from the 
thread that created it. You also can modify a thread's priority 
at any time after its creation by using the <code>setPriority</code> 
method. Thread priorities are integers ranging between 
<code>MIN_PRIORITY</code> and <code>MAX_PRIORITY</code> 
(constants defined in the <code>Thread</code> class). The 
higher the integer, the higher the priority. At any given 
time, when multiple threads are ready to be executed, the 
runtime system chooses for execution the Runnable thread 
that has the highest priority. Only when that thread stops, 
yields, or becomes Not Runnable will a lower-priority thread 
start executing. If two threads of the same priority are 
waiting for the CPU, the scheduler arbitrarily chooses one 
of them to run. The chosen thread runs until one of the 
following conditions is true: 
<ul>
<li>
A higher priority thread becomes runnable.
<li>
It yields, or its <code>run</code> method exits.
<li>
On systems that support time-slicing, its time allotment has expired.
</ul>
Then the second thread is given a chance to run, and so on, until the
interpreter exits.
<p>
The Java runtime system's thread scheduling algorithm is also preemptive.
If at any time a thread with a higher priority than all other
<code>Runnable</code> threads becomes <code>Runnable</code>,
the runtime system chooses the new higher-priority
thread for execution. The new thread is said to <em>preempt</em>
the other threads.
<p>

<blockquote><hr><strong>Rule of thumb:</strong>&nbsp; At any given time, the highest priority
thread is running. However, this is not guaranteed. The thread scheduler
may choose to run a lower priority thread to avoid starvation. For this
reason, use thread priority only to affect scheduling policy for efficiency
purposes. Do not rely on it for algorithm correctness.
<hr></blockquote>

</blockquote>
<h3>A Thread Race</h3>
<blockquote>


<a target="_blank" href="ex5/RaceApplet.java"><font color="#bb000f"><code>RaceApplet</code></font></a><a target="_blank" href="ex5/RaceApplet.java"><img src="../../images/sourceIcon.gif" width=11 height=11 border=0 align="MIDDLE" alt=" (in a .java source file)"></a> is
an applet that animates a race between two "runner" threads of
different priorities. Clicking the mouse on the applet
starts the two runners. Runner 2 has a priority of 2; 
runner 3 has a priority of 3. 
<blockquote><hr><strong>Try this:</strong>&nbsp;Click the applet below to start the race.
<hr></blockquote>

<applet code="RaceApplet.class" codebase="ex5" archive="raceapplet.jar" width="460" height="50"><param name="type" value="unfair"></applet>
<blockquote><hr><strong>Note:</strong> If you don't see the applet running above, you need to install Java Plug-in, which happens automatically when you <a href="http://java.sun.com/j2se/downloads.html" target="_blank">install the J2SE JRE or SDK</a>. This applet requires version 5.0 or later. You can find more information in the <a href="http://java.sun.com/products/plugin" target="_blank">Java Plug-in home page.</a><hr></blockquote>

<p>
The runners are implemented by a <code>Thread</code> subclass 
called <code>Runner</code>. Here is the <code>run</code> method 
for the <code>Runner</code> class, which simply counts from 
1 to 10,000,000: 

<blockquote><pre>
public int tick = 1;
public void run() {
    while (tick &lt; 10000000) {
        tick++;
    }
} 
</pre></blockquote>
This applet has a third thread, which handles the drawing. The 
drawing thread's <code>run</code> method loops until the applet 
stops. During each iteration of the loop, the thread draws a line 
for each runner, whose length is computed from the runner's 
<code>tick</code> variable; the thread then sleeps for 10 milliseconds. 
The drawing thread has a thread priority of 4 &#151; higher than that 
of either runner. Thus, whenever the drawing thread wakes up after 10 
milliseconds, it becomes the highest-priority thread, preempting 
whichever runner is currently running, and draws the lines. You can 
see the lines inch their way across the page.
<P>
This is not a fair race, because one runner has a higher priority 
than the other. Each time the drawing thread yields the CPU by going 
to sleep for 10 milliseconds, the scheduler chooses the highest-priority 
Runnable thread to run; in this case, it's always runner 3. 
<P>



Here is another applet, one that implements a fair race, 
in which both runners have the same priority and an equal 
chance of being chosen to run. 

<blockquote><hr><strong>Try this:</strong>&nbsp;Click the applet below to start the race.
<hr></blockquote>

<applet code="RaceApplet.class" codebase="ex5" archive="raceapplet.jar" width="460" height="50"><param name="type" value="fair"></applet>
<blockquote><hr><strong>Note:</strong> If you don't see the applet running above, you need to install Java Plug-in, which happens automatically when you <a href="http://java.sun.com/j2se/downloads.html" target="_blank">install the J2SE JRE or SDK</a>. This applet requires version 5.0 or later. You can find more information in the <a href="http://java.sun.com/products/plugin" target="_blank">Java Plug-in home page.</a><hr></blockquote>
In this race, each time the drawing thread yields the CPU by going 
to sleep, there are two Runnable threads of equal priority &#151; the 
runners &#151; waiting for the CPU. The scheduler must choose one 
of the threads to run. In this case, the scheduler arbitrarily 
chooses one.
</blockquote>

<h3>Selfish Threads</h3>
<blockquote>
The <code>Runner</code> class used in the previous races
implements impaired thread behavior.
Recall the <code>run</code> method from the <code>Runner</code> class used
in the races:
<blockquote><pre>
public int tick = 1;
public void run() {
    while (tick &lt; 10000000) {
        tick++;
    }
} 
</pre></blockquote>
The <code>while</code> loop in the <code>run</code> method is in 
a tight loop. Once the scheduler chooses a thread with this thread 
body for execution, the thread never voluntarily relinquishes 
control of the CPU; it just continues to run until the <code>while</code> 
loop terminates naturally or until the thread is preempted by a 
higher-priority thread. This thread is called a <I>selfish thread</I>.
<P>
In some cases, having selfish threads doesn't cause any problems, 
because a higher-priority thread preempts the selfish one, just as 
the drawing thread in <code>RaceApplet</code> preempts the selfish 
runners. However, in other cases, threads with CPU-greedy run methods 
can take over the CPU and cause other threads to wait for a long time, 
even forever, before getting a chance to run.
</blockquote>

<h3>Time-Slicing</h3>
<blockquote>
Some systems limit selfish-thread behavior with a strategy known 
as time slicing. Time slicing comes into play when multiple 
Runnable threads of equal priority are the highest-priority threads 
competing for the CPU. For example, a standalone program, 
<a target="_blank" href="ex5/RaceTest.java"><font color="#bb000f"><code>RaceTest.java</code></font></a><a target="_blank" href="ex5/RaceTest.java"><img src="../../images/sourceIcon.gif" width=11 height=11 border=0 align="MIDDLE" alt=" (in a .java source file)"></a>, based on

<a target="_blank" href="ex5/RaceApplet.java"><font color="#bb000f"><code>RaceApplet</code></font></a><a target="_blank" href="ex5/RaceApplet.java"><img src="../../images/sourceIcon.gif" width=11 height=11 border=0 align="MIDDLE" alt=" (in a .java source file)"></a> creates two equal-priority selfish threads that have this <code>run</code>
method:

<blockquote><pre>
public void run() {
    while (tick < 400000) {
        tick++;
        if ((tick % 50000) == 0) {
            System.out.println("Thread #" + num + ",
                               tick = " + tick);
        }
    }
}
</pre></blockquote>
This run method contains a tight loop that increments the integer 
<code>tick</code>. Every 50,000 ticks prints out the thread's 
identifier and its <code>tick</code> count. 
<P>
When running this program on a time-sliced system, you will see 
messages from both threads intermingled, something like this:
<blockquote><pre>
Thread #1, tick = 50000
Thread #0, tick = 50000
Thread #0, tick = 100000
Thread #1, tick = 100000
Thread #1, tick = 150000
Thread #1, tick = 200000
Thread #0, tick = 150000
Thread #0, tick = 200000
Thread #1, tick = 250000
Thread #0, tick = 250000
Thread #0, tick = 300000
Thread #1, tick = 300000
Thread #1, tick = 350000
Thread #0, tick = 350000
Thread #0, tick = 400000
Thread #1, tick = 400000
</pre></blockquote>
This output is produced because a <I>time-sliced system</I> divides the CPU into time slots and gives each equal-and-highest priority thread a time slot in which to run. The time-sliced system iterates through the equal-and-highest priority threads, allowing each one a bit of time to run, until one or more finishes or until a higher-priority thread preempts them. Note that time slicing makes no guarantees as to how often or in what order threads are scheduled to run. 
<P>
When running this program on a system that is not time sliced, you will see messages from one thread finish printing before the other thread ever gets a chance to print one message. The output will look something like this:
<blockquote><pre>
Thread #0, tick = 50000
Thread #0, tick = 100000
Thread #0, tick = 150000
Thread #0, tick = 200000
Thread #0, tick = 250000
Thread #0, tick = 300000
Thread #0, tick = 350000
Thread #0, tick = 400000
Thread #1, tick = 50000
Thread #1, tick = 100000
Thread #1, tick = 150000
Thread #1, tick = 200000
Thread #1, tick = 250000
Thread #1, tick = 300000
Thread #1, tick = 350000
Thread #1, tick = 400000
</pre></blockquote>
The reason is that a system that is not time sliced chooses one of the
equal-and-highest priority threads to run and allows that thread to run
until it relinquishes the CPU (by sleeping, yielding, or finishing its job)
or until a higher-priority preempts it.
<p>

<blockquote><hr><strong>Purity Tip:</strong>&nbsp;The Java platform does not implement (and therefore does not guarantee)
time slicing. However, some platforms do support time slicing.
Your programs should not rely on time slicing, as it may produce
different results on different systems.
<hr></blockquote>
<p>

</blockquote>
<h3>Relinquishing the CPU</h3>
<blockquote>
As you can imagine, writing CPU-intensive code can have negative 
repercussions on other threads running in the same process. In general, 
try to write well-behaved threads that voluntarily relinquish the CPU 
periodically and give other threads an opportunity to run.
<P>
A thread can voluntarily yield the CPU by calling the <code>yield</code> 
method. The <code>yield</code> method gives other threads of the same 
priority a chance to run. If no equal-priority threads are Runnable, 
the <code>yield</code> is ignored. 
</blockquote>

<h3>Thread Scheduling Summary</h3>
<ul>
<li>
Many computers have only one CPU, so threads must share the CPU with
other threads. The execution of multiple threads on a single CPU,
in some order, is called scheduling. The Java platform supports a simple,
deterministic scheduling algorithm called fixed-priority scheduling. 
<li>
Each thread has a numeric priority between <code>MIN_PRIORITY</code>
and <code>MAX_PRIORITY</code> (constants defined in the <code>Thread</code>
class). At any given time, when multiple threads are ready to be executed,
the highest-priority thread is chosen for execution.
Only when that thread stops or is suspended will a lower-priority thread
start executing.
<li>
When all the <code>Runnable</code> threads in the system have the same
priority, the scheduler arbitrarily chooses one of them to run.
<li>
The Java platform does not directly time slice. However, the system
implementation of threads underlying the Thread class may support time
slicing. Do not write code that relies on time slicing. 
<li>
A given thread may, at any time, give up its right to execute by calling
the <code>yield</code> method. Threads can yield the CPU only to other
threads of the same priority. Attempts to yield to a lower-priority
thread are ignored. 
</ul>
</blockquote>

<img src="../../images/blueline.gif" width="550" height="8" ALIGN="BOTTOM" ALT="">
<br>
<table width="550" summary="layout">
<tr>
<td align="left" valign="middle">
<a href="lifecycle.html" target="_top"><img src="../../images/PreviousArrow.gif" width="26" height="26" align="middle" border="0" alt="Previous Page"></a>
<a href="../TOC.html#threads" target="_top"><img src="../../images/TOCIcon.gif" width="26" height="26" align="middle" border="0" alt="Lesson Contents"></a>
<a href="multithreaded.html" target="_top"><img src="../../images/NextArrow.gif" width="26" height="26" align="middle" border="0" alt="Next Page"></a></td>

<td align="center" valign="middle">
<font size="-1">
<a href="../../index.html" target="_top">Start of Tutorial</a>
&gt;
<a href="../index.html" target="_top">Start of Trail</a>
&gt;
<a href="index.html" target="_top">Start of Lesson</a>
</font>
</td>

<td align="right" valign="middle">
<font size="-1">
<a href="../../search.html" target="_top">Search</a>
<br><a href="../../forms/sendusmail.html" target="_blank">Feedback Form</a>
</font>
</td>

</tr>
</table>
<p>
<font size="-1">
<a href="../../information/copyright.html">Copyright</a>
1995-2005 Sun Microsystems, Inc.  All rights reserved.
</font>
</p>
</body>
</html>
