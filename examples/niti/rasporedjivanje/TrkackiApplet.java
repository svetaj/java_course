import javax.swing.JApplet;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Container;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TrkackiApplet extends JApplet implements Runnable {

    private final static int BROJTRKACA = 2;
    private final static int RAZMAK = 40;

    private Trkac[] trkaci = new Trkac[BROJTRKACA];

    private Thread azurirajNit = null;

    public void init() {
        String tipTrke = getParameter("vrsta");
        for (int i = 0; i < BROJTRKACA; i++) {
            trkaci[i] = new Trkac();
            if (tipTrke.compareTo("neposteno") == 0)
                    trkaci[i].setPriority(i+2);
            else
                    trkaci[i].setPriority(2);
        }
        if (azurirajNit == null) {
            azurirajNit = new Thread(this, "Trka Niti");
            azurirajNit.setPriority(BROJTRKACA+2);
        }
        addMouseListener(new MyAdapter());
        setContentPane(new AppletContentPane());
    }

    class MyAdapter extends MouseAdapter {
        public void mouseClicked(MouseEvent evt) {
            if (!azurirajNit.isAlive())
                azurirajNit.start();
            for (int i = 0; i < BROJTRKACA; i++) {
                if (!trkaci[i].isAlive())
                    trkaci[i].start();
            }
        }
    }

    public void run() {
      Thread mojaNit = Thread.currentThread();
        while (azurirajNit == mojaNit) {
            repaint();
            try {
                Thread.sleep(3);
            } catch (InterruptedException e) { }
        }
    }

    public void stop() {
        for (int i = 0; i < BROJTRKACA; i++) {
            if (trkaci[i].isAlive())
                trkaci[i] = null;
        }
        if (azurirajNit.isAlive())
            azurirajNit = null;
    }

    class AppletContentPane extends JPanel {
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(Color.white);
            g.fillRect(0, 0, getSize().width, getSize().height);
            g.setColor(Color.black);
            for (int i = 0; i < BROJTRKACA; i++) {
                int pri = trkaci[i].getPriority();
                g.drawString(new Integer(pri).toString(), 0, (i+1)*RAZMAK);
            }
            for (int i = 0; i < BROJTRKACA; i++) {
                g.drawLine(RAZMAK, (i+1)*RAZMAK,
                 RAZMAK + (trkaci[i].puls)/1000, (i+1)*RAZMAK);
            }
        }
    }
}
