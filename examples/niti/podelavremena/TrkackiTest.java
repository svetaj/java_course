public class TrkackiTest {

    private final static int BROJTRKACA = 2;

    public static void main(String[] args) {
        SebicniTrkac[] trkaci = new SebicniTrkac[BROJTRKACA];

        for (int i = 0; i < BROJTRKACA; i++) {
            trkaci[i] = new SebicniTrkac(i);
            trkaci[i].setPriority(2);
        }
        for (int i = 0; i < BROJTRKACA; i++)
            trkaci[i].start();
    }
}
