import java.util.Timer;
import java.util.TimerTask;
import java.awt.Toolkit;

/**
 * Jednostavni demo koji koristi java.util.Timer za rasporedjivanje
 * zadatka jedanput nakon 5 sekundi.
 */

public class PodsetnikBip {
    Timer tajmer;
    Toolkit toolkit;

    public PodsetnikBip(int seconds) {
		toolkit = Toolkit.getDefaultToolkit();
        tajmer = new Timer();
        tajmer.schedule(new PodsetnikZadatak(), seconds*1000);
    }

    class PodsetnikZadatak extends TimerTask {
        public void run() {
            System.out.println("Vreme je!");
	        //tajmer.cancel(); //Nije neophodno zbog System.exit(0)
	        toolkit.beep();
	        toolkit.beep();
	        toolkit.beep();
	        toolkit.beep();
	        toolkit.beep();
            System.exit(0);   // Zaustavlja AWT nit
                              // (i sve ostalo)
	    }
    }

    public static void main(String args[]) {
	System.out.println("Pre pokretanja zadatka.");
        new PodsetnikBip(5);
	System.out.println("Zadatak pokrenut.");
    }
}
