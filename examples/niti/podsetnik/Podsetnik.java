import java.util.Timer;
import java.util.TimerTask;

/**
 * Jednostavni demo koji koristi java.util.Timer za rasporedjivanje
 * zadatka jedanput nakon 5 sekundi.
 */

public class Podsetnik {
    Timer tajmer;

    public Podsetnik(int seconds) {
        tajmer = new Timer();
        tajmer.schedule(new PodsetnikZadatak(), seconds*1000);
    }

    class PodsetnikZadatak extends TimerTask {
        public void run() {
            System.out.println("Vreme je!");
	    tajmer.cancel(); //Okoncanje niti tajmera
        }
    }

    public static void main(String args[]) {
	System.out.println("Pre pokretanja zadatka.");
        new Podsetnik(5);
	System.out.println("Zadatak pokrenut.");
    }
}
