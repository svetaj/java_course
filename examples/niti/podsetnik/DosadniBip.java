import java.util.Timer;
import java.util.TimerTask;
import java.awt.Toolkit;

/**
 * Jednostavni demo koji koristi java.util.Timer za rasporedjivanje
 * zadatka jedanput nakon 5 sekundi.
 */

public class DosadniBip {
    Timer tajmer;
    Toolkit alat;

    public DosadniBip(int seconds) {
		alat = Toolkit.getDefaultToolkit();
        tajmer = new Timer();
        tajmer.schedule(new DosadniZadatak(), 0, 1*1000);
    }

    class DosadniZadatak extends TimerTask {
        int brojUpozorenja = 3;
        public void run() {
            if (brojUpozorenja > 0) {
                alat.beep();
                System.out.println("Tatararira!");
                brojUpozorenja--;
            } else {
                alat.beep();
                System.out.println("vreme je!");
                //tajmer.cancel(); // Nije neophodno
                System.exit(0);   // Zaustavlja AWT nit
                                  // (i sve ostalo)
            }
        }
    }

    public static void main(String args[]) {
	System.out.println("Pre pokretanja zadatka.");
        new DosadniBip(5);
	System.out.println("Zadatak pokrenut.");
    }
}
