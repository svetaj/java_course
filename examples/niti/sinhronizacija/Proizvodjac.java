class Proizvodjac extends Thread {
    private Rupa rupa;
    private int broj;

    public Proizvodjac(Rupa c, int broj) {
        rupa = c;
        this.broj = broj;
    }

    public void run() {
        for (int i = 0; i < 10; i++) {
            rupa.stavi(i);
            System.out.println("   Proizvodjac #" + this.broj + " stavio: " + i);
            try {
                sleep((int)(Math.random() * 100));
            } catch (InterruptedException e) {
            }
        }
    }
}