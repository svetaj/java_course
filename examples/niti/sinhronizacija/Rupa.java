class Rupa {
    private int sadrzaj;
    private boolean raspolozivo = false;

    public synchronized int uzmi() {
        while (raspolozivo == false) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        raspolozivo = false;
        notifyAll();
        return sadrzaj;
    }

    public synchronized void stavi(int vrednost) {
        while (raspolozivo == true) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        sadrzaj = vrednost;
        raspolozivo = true;
        notifyAll();
    }
}