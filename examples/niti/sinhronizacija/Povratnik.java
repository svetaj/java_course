class TestPovratnik {
   public static void main(String[] args) {
       Povratnik x = new Povratnik();
       x.a();
   }
}

class Povratnik {
    public synchronized void a() {
	b();
	System.out.println("Ja sam u a()");
    }
    public synchronized void b() {
	System.out.println("Ja sam u b()");
    }
}
