class Potrosac extends Thread {
    private Rupa rupa;
    private int broj;

    public Potrosac (Rupa c, int broj) {
        rupa = c;
        this.broj = broj;
    }

    public void run() {
        int vrednost = 0;
        for (int i = 0; i < 10; i++) {
            vrednost = rupa.uzmi();
            System.out.println("XXX Potrosac #" + this.broj + " uzeo: " + vrednost);
        }
    }
}