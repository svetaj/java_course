import java.awt.*;
import java.util.*;
import java.applet.*;
import java.text.*;

public class Sat extends java.applet.Applet implements Runnable {
    private volatile Thread clockThread = null;
    DateFormat formatDatuma;     	// Formatira prikazani datum
    String poslednjiDatum;          // String u koji se upisuje datum
    Date trenutniDatum;         	// Koristi se za dobijanje datuma
    Color bojaCifara;        		// Boja brojeva
    Font fontSata;
    Locale vrednostLocale;

    public void init() {
        setBackground(Color.white);
        bojaCifara = Color.red;
        vrednostLocale = vrednostLocale.getDefault();
        formatDatuma =
            DateFormat.getDateTimeInstance(DateFormat.FULL,
            DateFormat.MEDIUM, vrednostLocale);
        trenutniDatum = new Date();
        poslednjiDatum = formatDatuma.format(trenutniDatum);
        fontSata = new Font("Sans-Serif",
                                 Font.PLAIN, 14);
        resize(275,25);
   }

    public void start() {
      if (clockThread == null) {
         clockThread = new Thread(this, "Sat");
         clockThread.start();
      }
    }

    public void run() {
      Thread myThread = Thread.currentThread();
        while (clockThread == myThread) {
            repaint();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e){ }
        }
    }

    public void paint(Graphics g) {
      String danas;
      trenutniDatum = new Date();
      formatDatuma =
           DateFormat.getDateTimeInstance(DateFormat.FULL,
           DateFormat.MEDIUM, vrednostLocale);
      danas = formatDatuma.format(trenutniDatum);
      g.setFont(fontSata);

      // Obrisi i ponovo nacrtaj
      g.setColor(getBackground());
      g.drawString(poslednjiDatum, 0, 12);

      g.setColor(bojaCifara);
      g.drawString(danas, 0, 12);
      poslednjiDatum = danas;
      trenutniDatum=null;

    }

    public void stop() {
        clockThread = null;
    }
}

