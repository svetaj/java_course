/** 
 * Klasa ZdravoApp prikazuje niz "Zdravo" 
 * standardnom izlazu.
 */

class ZdravoApp {
    public static void main(String[] args) {
        System.out.println("Zdravo !");        //Prikazuje zadati niz
    }
}
