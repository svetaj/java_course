/**
 * Tacka.java koristi se u KlikniMe.java.
 */

public class Tacka {
    //promenljive instance
    private int velicina;
    public int x, y;

    //konstruktor
    public Tacka() {
        x = -1;
        y = -1;
        velicina = 1;
    }

    //metodi za pristup velicini promenljive instance
    public void postaviVelicinu(int novaVelicina) {
        if (novaVelicina >= 0) {
            velicina = novaVelicina;
        }
    }
    public int uzmiVelicinu() {
        return velicina;
    }
}
