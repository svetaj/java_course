/**
 * KlikniMe.java se koristi u KlikniMeApp.java.  KlikniMe je
 * komponenta koja stavlja tacku tamo gde kliknemo.  Zahteva
 * drugi fajl: Tacka.java.
 */
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import java.awt.*;
import java.awt.event.*;

public class KlikniMe extends JComponent
                     implements MouseListener {
    private Tacka spot = null;
    private static final int RADIUS = 7;
    private Color spotColor = new Color(107, 116, 2); //maslinasto

    /** kreira i inicijalizuje KlikniMe komponentu. */
    public KlikniMe() {
        addMouseListener(this);

        //Postavljanje velicina komponente.
        setPreferredSize(new Dimension(RADIUS * 30, RADIUS * 15));
        setMinimumSize(new Dimension(RADIUS * 4, RADIUS * 4));

        //Zahteva crnu liniju oko komponente.
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }

    /**
     * Crta KlikniMe komponentu.  Ovaj metod
     * uvodi Swing sistem za crtanje komponenti.
     */
    public void paintComponent(Graphics g) {
        /**
         * Kopira graficki kontekst tako da mozemo da ga menjamo.
         * Pretvaranje u Graphics2D tako da mozemo da koristimo antialiasing.
         */
        Graphics2D g2d = (Graphics2D)g.create();

        //Ukljucuje antialiasing, tako da je crtanje nezno.
        g2d.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        //Crta pozadinu.
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, getWidth() - 1, getHeight() - 1);

        //Crta tacku.
        if (spot != null) {
            int radius = spot.uzmiVelicinu();
            g2d.setColor(spotColor);
            g2d.fillOval(spot.x - radius, spot.y - radius,
                         radius * 2, radius * 2);
        }
    }

    //Methodi koje zahteva MouseListener interfejs.
    public void mousePressed(MouseEvent event) {
        if (spot == null) {
            spot = new Tacka();
            spot.postaviVelicinu(RADIUS);
        }
        spot.x = event.getX();
        spot.y = event.getY();
        repaint();
    }
    public void mouseClicked(MouseEvent event) {}
    public void mouseReleased(MouseEvent event) {}
    public void mouseEntered(MouseEvent event) {}
    public void mouseExited(MouseEvent event) {}
}
