/**
 * KlikniMeApp.java je aplikacija koja se prevodi i izvrsava u
 * J2SE v5.0.  Zahteva dva dodatna fajla:
 *   KlikniMe.java
 *   Tacka.java
 */

import javax.swing.SwingUtilities;
import javax.swing.JLabel;
import javax.swing.JFrame;
import java.awt.BorderLayout;

public class KlikniMeApp implements Runnable {

    /**
     * Ovaj konstruktor kreira instancu od ClickMeApp,
     * koji kreira i prikazuje prozor koji sadrzi
     * KlikniMe komponentu.
     */
    public KlikniMeApp() {
        /*
         * Kazuje niti za raspodelu dogadjaja (koristi se za
         * prikazivanje i prihvat dogadjaja Swing GUI-a)
         * da pozove run metod od "this" (KlikniMeApp
         * objekat). Argument invokeLater mora da implementira
         * Runnable interfejs koji garantuje da definise run metod
         */
        SwingUtilities.invokeLater(this);
    }

    /**
     * Kreira i prikazuje GUI. Ovaj metod treba da se uvede
     * u niti za raspodelu dogadjaja.
     */
    public void run() {
        createAndShowGUI();
    }

    /**
     * Otvara prozor koji sadrzi ClickMe komponentu.
     * Rad bezbednosti niti, ovaj metod treba da se uvede iz
     * niti za raspodelu dogadjaja.
     */
    private static void createAndShowGUI() {
        //Leo dekorisanje prozora.
        JFrame.setDefaultLookAndFeelDecorated(true);

        //Kreira i postavlja prozor.
        JFrame frame = new JFrame("KlikniMeApp");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Postavlja menadzer rasporeda.
        frame.getContentPane().setLayout(new BorderLayout());
        // frame.setLayout(new BorderLayout());

        //Dodaje KlikniMe komponentu.
        KlikniMe component = new KlikniMe();
        // frame.add(component, BorderLayout.CENTER);
        frame.getContentPane().add(component, BorderLayout.CENTER);

        //Dodaje labelu sa objasnjenjem.
//        frame.add(new JLabel("Klikni unutar pravougaonika."),
//                             BorderLayout.SOUTH);
        frame.getContentPane().add(new JLabel("Klikni unutar pravougaonika."),
                             BorderLayout.SOUTH);

        //Prikazi prozor.
        frame.pack();
        frame.setVisible(true);
    }

    //Ovaj metod se automatski izvrsava.
    public static void main(String[] args) {
        //Kreira instancu od KlikniMeApp.
        new KlikniMeApp();
    }
}
