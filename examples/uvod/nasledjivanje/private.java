class Alfa {
    private int x_privatno;
    private void privatniMetod() {
        System.out.println("privatniMethod");
    }
    boolean jeJednakoSa(Alfa drugoAlfa) {
        if (this.x_privatno == drugoAlfa.x_privatno)
            return true;
        else
            return false;
    }
    Alfa(int i) {
        x_privatno = i;
        System.out.println("Konstruktor, x_privatno = " + x_privatno);
    }
}


class PrivatnaApp {
    public static void main(String[] args) {
        Alfa trt = new Alfa(1);
        Alfa mrt = new Alfa(1);
        if (trt.jeJednakoSa(mrt) == false)
             System.out.println("Nije jednako");
        else
             System.out.println("Jednako");
    }
}
