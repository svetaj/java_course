class GlupaKlasa {
    boolean promenljiva;
    void metod() {
        promenljiva = true;
    }
}


class GlupljaKlasa extends GlupaKlasa {
    boolean promenljiva;
    void metod() {
        promenljiva = false;
        super.metod();
        System.out.println(promenljiva);
        System.out.println(super.promenljiva);
    }
}

class SuperApp {
    public static void main(String[] args) {
        GlupljaKlasa ttt = new GlupljaKlasa();
        ttt.metod();
    }
}

