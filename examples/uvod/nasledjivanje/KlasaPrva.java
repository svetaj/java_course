/* Ova klasa koristi API uveden u 1.1. */

import java.util.Locale;

public class KlasaPrva {
    public String uzmiString() {
        return Locale.getDefault().getDisplayName();
    }
}
