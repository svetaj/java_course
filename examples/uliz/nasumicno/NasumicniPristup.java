import java.io.*;

class NasumicniPristup {
    public static void main(String[] args) throws IOException {
       String tekst = "Dodaj ovaj tekst na kraj fajla";
       RandomAccessFile fajl = new RandomAccessFile("fajl.txt", "rw");
       fajl.skipBytes((int)fajl.length());
       fajl.writeBytes(tekst);
       fajl.close();
    }
}
