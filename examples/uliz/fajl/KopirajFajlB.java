import java.io.*;

public class KopirajFajlB {
    public static void main(String[] args) throws IOException {
        File ulazniFajl = new File("stari.txt");
        File izlazniFajl = new File("novi.txt");

        FileInputStream ul = new FileInputStream(ulazniFajl);
        FileOutputStream iz = new FileOutputStream(izlazniFajl);
        int c;

        while ((c = ul.read()) != -1)
           iz.write(c);

        ul.close();
        iz.close();
    }
}
