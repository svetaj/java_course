import java.io.*;

public class KopirajFajl {
    public static void main(String[] args) throws IOException {
	File ulazniFajl = new File("stari.txt");
	File izlazniFajl = new File("novi.txt");

        FileReader ul = new FileReader(ulazniFajl);
        FileWriter iz = new FileWriter(izlazniFajl);
        int c;

        while ((c = ul.read()) != -1)
           iz.write(c);

        ul.close();
        iz.close();
    }
}
