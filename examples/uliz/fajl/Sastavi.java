import java.io.*;

public class Sastavi {
    public static void main(String[] args) throws IOException {
        ListaFajlova mojalista = new ListaFajlova(args);

        SequenceInputStream s = new SequenceInputStream(mojalista);
        int c;

        while ((c = s.read()) != -1)
           System.out.write(c);

        s.close();
    }
}
