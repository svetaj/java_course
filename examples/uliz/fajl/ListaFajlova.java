import java.util.*;
import java.io.*;

public class ListaFajlova implements Enumeration {

    private String[] lista;
    private int tekuci = 0;

    public ListaFajlova(String[] lista) {
        this.lista = lista;
    }

    public boolean hasMoreElements() {
        if (tekuci < lista.length)
            return true;
        else
            return false;
    }

    public Object nextElement() {
        InputStream ul = null;

        if (!hasMoreElements())
            throw new NoSuchElementException("Nema vise fajlova.");
        else {
            String sledeciElement = lista[tekuci];
            tekuci++;
            try {
                ul = new FileInputStream(sledeciElement);
            } catch (FileNotFoundException e) {
                System.err.println("ListaFajlova: Ne mogu da otvorim " + sledeciElement);
            }
        }
        return ul;
    }
}

