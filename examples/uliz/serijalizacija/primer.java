import java.util.*;
import java.io.*;

class Abc1
{
	public static void main (String[] args)
	throws FileNotFoundException, IOException, ClassNotFoundException {
		FileOutputStream out = new FileOutputStream("Vreme.velja");
		ObjectOutputStream s = new ObjectOutputStream(out);
		s.writeObject("Velja");
		Date x = new Date();
		s.writeObject(x);
		s.flush();
		System.out.println (x.toString());
	}
}

class Abc2
{
	public static void main (String[] args)
	throws FileNotFoundException, IOException,  ClassNotFoundException {

		FileInputStream in = new FileInputStream("Vreme.velja");
		ObjectInputStream s = new ObjectInputStream(in);
		String today = (String)s.readObject();
		Date y = (Date)s.readObject();
		System.out.println (y.toString());
	}
}