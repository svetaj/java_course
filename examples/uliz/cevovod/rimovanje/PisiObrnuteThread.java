import java.io.*;

class PisiObrnuteThread extends Thread {
     PrintStream ps;
     DataInputStream dis;

     PisiObrnuteThread(PrintStream ps, DataInputStream dis) {
        this.ps = ps;
        this.dis = dis;
     }

     public void run() {
        if (ps != null && dis != null) {
           try {
                String linija;
                while ((linija = dis.readLine()) != null) {
                        ps.println(ObrniGu(linija));
                        ps.flush();
                }
                ps.close();
           } catch (IOException e) {
                System.out.println("PisiObrnuteThread.run() " + e);
           }
        }
     }

     protected void finalize() {
        try {
           if (ps != null) {
                 ps.close();
                ps = null;
           }
           if (dis != null) {
                dis.close();
                dis = null;
           }
        } catch (IOException e) {
                System.out.println("PisiObrnuteThread.finalize() " + e);
        }
     }

     private String ObrniGu (String izvor) {
          int i, duzina = izvor.length();

          StringBuffer izl = new StringBuffer(duzina);

          for (i = (duzina - 1) ; i >= 0; i--) {
                izl.append(izvor.charAt(i));
          }
         return izl.toString();
     }
}

