import java.io.*;

class PrebrojFajl {
    public static void main(String[] args)
        throws java.io.IOException, java.io.FileNotFoundException
    {
        int brojac = 0;
	InputStream is;
        String imefajla;

	if (args.length >=1) {
		is = new FileInputStream(args[0]);
                imefajla = args[0];
	} else {
		is = System.in;
                imefajla = "Ulaz";
	}
        while (is.read() != -1)
            brojac++;
        System.out.println(imefajla + " ima " + brojac + " karaktera.");
    }
}
