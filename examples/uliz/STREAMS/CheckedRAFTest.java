import java.io.*;

class CheckedRAFTest {
    public static void main(String[] args) {

        Adler32 inChecker = new Adler32();
        Adler32 outChecker = new Adler32();
        CheckedDataInput cis = null;
        CheckedDataOutput cos = null;

        try {
            cis = new CheckedDataInput(new RandomAccessFile("farrago.txt", "r"), inChecker);
            cos = new CheckedDataOutput(new RandomAccessFile("outagain.txt", "rw"), outChecker);
        } catch (FileNotFoundException e) {
            System.err.println("CheckedIOTest: " + e);
            System.exit(-1);
        } catch (IOException e) {
            System.err.println("CheckedIOTest: " + e);
            System.exit(-1);
        }

        try {
            boolean EOF = false;

            while (!EOF) {
                try {
                    int c = cis.readByte();
                    cos.write(c);
                } catch (EOFException e) {
                    EOF = true;
                }
            }

            System.out.println("Input stream check sum: " + cis.getChecksum().getValue());
            System.out.println("Output stream check sum: " + cos.getChecksum().getValue());
        } catch (IOException e) {
            System.err.println("CheckedIOTest: " + e);
        }
    }
}