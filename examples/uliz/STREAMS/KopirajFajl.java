import java.io.*;

class KopirajFajl {
        public static void main(String[] args) {
          try {         
             File  ulaz = new File("farrago.txt");
             File izlaz = new File("outtext.txt");

             FileInputStream fis = new FileInputStream(ulaz);
             FileOutputStream fos = new FileOutputStream(izlaz);

             int c;
             while ((c = fis.read()) != -1) {
                  fos.write(c);
             }

             fis.close();
             fos.close();
          } catch (FileNotFoundException e) {
                System.out.println("greska : " + e);
          } catch (IOException e) {
                System.out.println("greska : " + e);
          }
      }
}
