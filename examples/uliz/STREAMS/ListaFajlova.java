import java.io.*;
import java.util.*;

class ListaFajlova implements Enumeration {
    String[] lista;
    int tekuci = 0;

    ListaFajlova(String[] lista) {
         this.lista = lista;
    }

    public boolean hasMoreElements() {
       if (tekuci < lista.length)
            return true;
       else
            return false;
    }

    public Object nextElement() {
        InputStream is = null;

        if (!hasMoreElements()) {
            throw new NoSuchElementException("nema vise fajlova");
        } else {
            try {
                String sledeci = lista[tekuci];
                tekuci++;
                is = new FileInputStream(sledeci);
            } catch (FileNotFoundException e) {
                System.out.println("Greska : " + e);
            }
        }
        return is;
    }
}
