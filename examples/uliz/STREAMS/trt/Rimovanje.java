import java.io.*;

class Rimovanje {
        public static void main(String[] args) {
            try {
                DataInputStream reci = new DataInputStream(new FileInputStream("words.txt"));

                InputStream RimovaneReci = obrni(sortiraj(obrni(reci)));

                DataInputStream dis = new DataInputStream(RimovaneReci);

                String linija;

                while ((linija = dis.readLine()) != null) {
                        System.out.println(linija);
                }
                dis.close();
            } catch (Exception e) {
                System.out.println("Rimovanje reci: " + e);
            }
        }

        public static InputStream obrni (InputStream izvor) {
            PipedOutputStream pos = null;
            PipedInputStream pis = null;

            try {
                DataInputStream dis = new DataInputStream(izvor);

                pos = new PipedOutputStream();
                pis = new PipedInputStream(pos);
                PrintStream ps = new PrintStream(pos);

                new PisiObrnuteThread(ps, dis).start();
            } catch (Exception e) {
                System.out.println("obrni: " + e);
            }    
            return pis;
        }

        public static InputStream sortiraj (InputStream izvor) {
            PipedInputStream pis = null;
            PipedOutputStream pos = null;

            try {
                DataInputStream dis = new DataInputStream(izvor);

                pos = new PipedOutputStream();
                pis = new PipedInputStream(pos);
                PrintStream ps = new PrintStream(pos);

                new SortirajThread(ps, dis).start();
            } catch (Exception e) {
                System.out.println("obrni: " + e);
            }    
            return pis;
        }
}


