import java.io.*;

class RhymingWords {
    public static void main(String[] args) {

        try {
            DataInputStream words = new DataInputStream(new FileInputStream("words.txt"));

                // do the reversing and sorting
            InputStream rhymedWords = reverse(sort(reverse(words)));

                // write new list to standard out
            DataInputStream dis = new DataInputStream(rhymedWords);
            String input;

            while ((input = dis.readLine()) != null) {
                System.out.println(input);
            }
            dis.close();

        } catch (Exception e) {
            System.out.println("RhymingWords: " + e);
        }
    }

    public static InputStream reverse(InputStream source) {
        PipedOutputStream pos = null;
        PipedInputStream pis = null;

        try {
            DataInputStream dis = new DataInputStream(source);

            pos = new PipedOutputStream();
            pis = new PipedInputStream(pos);
            PrintStream ps = new PrintStream(pos);

            new WriteReversedThread(ps, dis).start();

        } catch (Exception e) {
            System.out.println("RhymingWords reverse: " + e);
        }
        return pis;
    }

    public static InputStream sort(InputStream source) {
        PipedOutputStream pos = null;
        PipedInputStream pis = null;

        try {
            DataInputStream dis = new DataInputStream(source);

            pos = new PipedOutputStream();
            pis = new PipedInputStream(pos);
            PrintStream ps = new PrintStream(pos);

            new SortThread(ps, dis).start();

        } catch (Exception e) {
            System.out.println("RhymingWords sort: " + e);
        }
        return pis;
    }
}