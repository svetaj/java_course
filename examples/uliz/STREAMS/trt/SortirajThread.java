import java.io.*;

class SortirajThread extends Thread {
     PrintStream ps;
     DataInputStream dis;

     SortirajThread(PrintStream ps, DataInputStream dis) {
        this.ps = ps;
        this.dis = dis;
     }

     public void run() {
        int MAXLINIJA = 50;
        if (ps != null && dis != null) {
           try {
                String[] linije = new String[MAXLINIJA];
                int brlinija = 0, i = 0;
                while ((linije[brlinija] = dis.readLine()) != null) {
                        brlinija++;
                }
                brzisort(linije, 0, brlinija-1);
                for (i = 0; i < brlinija; i++) {
                    ps.println(linije[i]);
                }
                ps.close();
           } catch (IOException e) {
                System.out.println("SortirajThread.run() " + e);
           }
        }
     }

     protected void finalize() {
        try {
           if (ps != null) {
                 ps.close();
                ps = null;
           }
           if (dis != null) {
                dis.close();
                dis = null;
           }
        } catch (IOException e) {
                System.out.println("SortirajThread.finalize() " + e);
        }
     }

     private static void brzisort(String[] t, int po0, int kr0)
     {
        int po = po0;
        int kr = kr0;

        if (po >= kr) {
           return;
        }

        String sr = t[(po + kr) /2];

        while (po < kr) {
            while (po < kr && t[po].compareTo(sr) < 0) {
                po++;
            }
            while (po < kr && t[kr].compareTo(sr) > 0) {
                kr--;
            }
            if (po < kr) {
               String T = t[po];
               t[po] = t[kr];
               t[kr] = T;
            }
        }

        if (kr < po) {
             int T = po;
             po = kr;
             kr = T;
        }

        brzisort(t, po0, po);
        brzisort(t, (po0 == po) ? po + 1 : po, kr0);
     }
}

