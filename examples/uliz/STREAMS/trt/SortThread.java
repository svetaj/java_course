import java.io.*;

class SortThread extends Thread {
    PrintStream ps;
    DataInputStream dis;

    SortThread(PrintStream ps, DataInputStream dis) {
        this.ps = ps;
        this.dis = dis;
    }

    public void run() {
         int MAXWORDS = 50;

        if (ps != null && dis != null) {
            try {
                String[] listOfWords = new String[MAXWORDS];
                int numwords = 0, i = 0;
 
                while ((listOfWords[numwords] = dis.readLine()) != null) {
                    numwords++;
                }
                quicksort(listOfWords, 0, numwords-1);
                for (i = 0; i < numwords; i++) {
                    ps.println(listOfWords[i]);
                }
                ps.close();
            } catch (IOException e) {
                System.out.println("WriteReversedThread run: " + e);
            }
        }
    }

    protected void finalize() {
        try {
            if (ps != null) {
                ps.close();
                ps = null;
            }
            if (dis != null) {
                dis.close();
                dis = null;
            }
        } catch (IOException e) {
            System.out.println("WriteReversedThread finalize: " + e);
        }
    }

    private static void quicksort(String[] a, int lo0, int hi0) {
        int lo = lo0;
        int hi = hi0;
        if (lo >= hi) {
            return;
        }
        String mid = a[(lo + hi) / 2];
        while (lo < hi) {
            while (lo<hi && a[lo].compareTo(mid) < 0) {
                lo++;
            }
            while (lo<hi && a[hi].compareTo(mid) > 0) {
                hi--;
            }
            if (lo < hi) {
                String T = a[lo];
                a[lo] = a[hi];
                a[hi] = T;
            }
        }
        if (hi < lo) {
            int T = hi;
            hi = lo;
            lo = T;
        }
        quicksort(a, lo0, lo);
        quicksort(a, lo == lo0 ? lo+1 : lo, hi0);
    }
}