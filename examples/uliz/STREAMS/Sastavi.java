import java.io.*;

class Sastavi {
        public static void main(String[] args) {
                ListaFajlova lf = new ListaFajlova(args);
           try {
                SequenceInputStream sis = new SequenceInputStream(lf);

                int c;
                while ((c = sis.read()) != -1) {
                        System.out.write(c);
                }

                sis.close();
           } catch (IOException e) {
                System.err.println(" Greska : " + e);
           }
       }
}
