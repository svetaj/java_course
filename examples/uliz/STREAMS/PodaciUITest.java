import java.io.*;

class PodaciIOTest {
     public static void main(String[] args) {
                // pisanje
            try {
                 DataOutputStream dos = new DataOutputStream(new FileOutputStream("racun1.txt"));
                 double[] cene = { 19.99, 9.99, 15.99, 3.99, 4.99 };
                 int[] jedinice = { 12, 8, 13, 29, 50 };
                 String[] opisi = { "Java gace", "Java solja", "kilo leba", "Java nosa", "Java kurton" };

                 for (int i = 0; i < cene.length; i++) {
                      dos.writeDouble(cene[i]);
                      dos.writeChar('\t');
                      dos.writeInt(jedinice[i]);
                      dos.writeChar('\t');
                      dos.writeChars(opisi[i]);
                      dos.writeChar('\n');
                 }
                 dos.close();
            } catch (IOException e) {
                System.out.println("Greska : " + e);
            }


            // citanje

            try {
                DataInputStream dis = new DataInputStream(new FileInputStream("racun1.txt"));

                double cijena;
                int jed;
                String op;
                double total = 0.0;

                try {
                        while (true) {
                            cijena = dis.readDouble();
                            dis.readChar();
                            jed = dis.readInt();
                            dis.readChar();
                            op = dis.readLine();
                            total += jed * cijena;
                            System.out.println("Narucili ste " + jed + " tj.  " + op + " po ceni od" + cijena);
                        }
                } catch (EOFException e) {
                }
                System.out.println("Ukupna vrednost je: $" + total);

            } catch (FileNotFoundException e) {
                System.out.println("Greska : " + e);
            } catch (IOException e) {
                System.out.println("Greska : " + e);
            }
     }
}

