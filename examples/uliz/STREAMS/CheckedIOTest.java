import java.io.*;

class CheckedIOTest {
    public static void main(String[] args) {

       Adler32 inChecker = new Adler32();
       Adler32 outChecker = new Adler32();
       CheckedInputStream cis = null;
       CheckedOutputStream cos = null;

       try {
           cis = new CheckedInputStream(new FileInputStream("farrago.txt"), inChecker);
           cos = new CheckedOutputStream(new FileOutputStream("outagain.txt"), outChecker);
       } catch (FileNotFoundException e) {
           System.err.println("CheckedIOTest: " + e);
           System.exit(-1);
       } catch (IOException e) {
           System.err.println("CheckedIOTest: " + e);
           System.exit(-1);
       }

       try {
           int c;

           while ((c = cis.read()) != -1) {
              cos.write(c);
           }

           System.out.println("Input stream check sum: " + inChecker.getValue());
           System.out.println("Output stream check sum: " + outChecker.getValue());

           cis.close();
           cos.close();
       } catch (IOException e) {
           System.err.println("CheckedIOTest: " + e);
       }
    }
}