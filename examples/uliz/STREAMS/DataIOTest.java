import java.io.*;

class DataIOTest {
    public static void main(String[] args) {

            // writing part
        try {
            DataOutputStream dos = new DataOutputStream(new FileOutputStream("invoice1.txt"));

            double[] prices = { 19.99, 9.99, 15.99, 3.99, 4.99 };
            int[] units = { 12, 8, 13, 29, 50 };
            String[] descs = { "Java T-shirt", "Java Mug", "Duke Juggling Dolls", "Java Pin", "Java Key Chain" };
        
            for (int i = 0; i < prices.length; i ++) {
                dos.writeDouble(prices[i]);
                dos.writeChar('\t');
                dos.writeInt(units[i]);
                dos.writeChar('\t');
                dos.writeChars(descs[i]);
                dos.writeChar('\n');
            }
            dos.close();
        } catch (IOException e) {
            System.out.println("DataIOTest: " + e);
        }

            // reading part
        try {
            DataInputStream dis = new DataInputStream(new FileInputStream("invoice1.txt"));

            double price;
            int unit;
            String desc;
            double total = 0.0;

            try {
                while (true) {
                    price = dis.readDouble();
                    dis.readChar();       // throws out the tab
                    unit = dis.readInt();
                    dis.readChar();       // throws out the tab
                    desc = dis.readLine();
                    System.out.println("You've ordered " + unit + " units of " + desc + " at $" + price);
                    total = total + unit * price;
                }
            } catch (EOFException e) {
            }
            System.out.println("For a TOTAL of: $" + total);
            dis.close();
        } catch (FileNotFoundException e) {
            System.out.println("DataIOTest: " + e);
        } catch (IOException e) {
            System.out.println("DataIOTest: " + e);
        }
    }
}