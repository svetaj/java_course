import java.io.*;

public class PodaciUIDemo {
    public static void main(String[] args) throws IOException {

        // upisuje formatizovane podatke u fajl
        DataOutputStream out = new DataOutputStream(new
				   FileOutputStream("narudzba.txt"));

        double[] cene = { 19.99, 9.99, 15.99, 3.99, 4.99 };
        int[] kolicina = { 12, 8, 13, 29, 50 };
        String[] opisi = { "Java majca",
			   "Java gace",
			   "Java pidzama",
			   "Java solja",
			   "Java kajla" };

        for (int i = 0; i < cene.length; i ++) {
            out.writeDouble(cene[i]);
            out.writeChar('\t');
            out.writeInt(kolicina[i]);
            out.writeChar('\t');
            out.writeChars(opisi[i]);
            out.writeChar('\n');
        }
        out.close();

        // ponovno citanje formatizovanih podataka iz fajla
        DataInputStream ul = new DataInputStream(new
				 FileInputStream("narudzba.txt"));

        double cena;
        int komada;
        StringBuffer opis;
        double total = 0.0;

        try {
            while (true) {
                cena = ul.readDouble();
                ul.readChar();       // odbacuje tabulator
                komada = ul.readInt();
                ul.readChar();       // odbacuje tabulator
		char znak;
		opis = new StringBuffer(20);
		char separator = System.getProperty("line.separator").charAt(0);
		while ((znak = ul.readChar()) != separator)
		    opis.append(znak);
                System.out.println("Naruceno je " +
				    komada + " komada " +
				    opis + " po ceni " + cena);
                total = total + komada * cena;
            }
        } catch (EOFException e) {
        }
        System.out.println("Ukupna cena: " + total);
        ul.close();
    }
}
