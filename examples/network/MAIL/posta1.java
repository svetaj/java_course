// PC PRESS 23 APR 97
// posta,java
import java.io.*;
import java.net.*;

public class posta1 {//CLASS
	public static void main(String [] args) {//MAIN
		String linija, linija1;
      byte[] trt = new byte[40];
		Socket klijent = null;
		DataOutputStream upis = null;
		DataInputStream citanje = null;
		DataInputStream dis = null;
		DataInputStream dis1 = null;
		FileInputStream fin = null;
		FileInputStream fin1 = null;
      String odg = null;

		try {
			klijent = new Socket(args[2], 25);
		} 
		catch (UnknownHostException e) {
			System.out.println ("Ne mogu da nadjem host");
			System.out.println (e);
		} 
		catch (IOException e) {
			System.out.println ("I/O konekcija neuspesna");
			System.out.println (e);
		}
		try {
			upis = new DataOutputStream (klijent.getOutputStream());
			citanje = new DataInputStream (klijent.getInputStream());
		}
		catch (IOException e) {
			System.out.println ("I/O konekcija neuspesna");
			System.out.println (e);
		}
		if(klijent != null && upis != null && citanje != null) {//IF
			try {
				fin = new FileInputStream(args[0]);
			}
			catch (Exception e) {
				System.out.println("Greska: " + e);
			}
			try {
				dis = new DataInputStream(fin);
			}
			catch (Exception e) {
				System.out.println("Greska: " + e);
			}
			try {//try-WHILE1
				while ((linija = dis.readLine()) != null) {//WHILE1
					try {
                      citanje.read(trt);
                      odg = new String(trt);            
                      System.out.println("STAR:" + odg);
 					   upis.writeBytes("HELO\n");
  						    citanje.read(trt);
                      odg = new String(trt);            
                      System.out.println("HELO:" + odg);
 					   upis.writeBytes("MAIL From: <" + args[3] + ">\n");
  						    citanje.read(trt);
                      odg = new String(trt);            
                      System.out.println("MAIL:" + odg);
						upis.writeBytes("RCPT To: <" + linija + ">\n");
                      citanje.read(trt);
                      odg = new String(trt);            
                      System.out.println("RCPT:" + odg);
						upis.writeBytes("DATA\n");
                      odg = new String(trt);            
                      citanje.read(trt);
                      System.out.println("DATA:" + odg);
						upis.writeBytes("From: <" + args[3] + ">\n");
						upis.writeBytes("To: <" + linija + ">\n");
						upis.writeBytes("Subject: " + args[4] + "\n");
					}
					catch (IOException e) {
						System.out.println ("Greska: " + e);
					}
					try {
						fin1 = new FileInputStream(args[1]);
					}
					catch (Exception e) {
						System.out.println("Greska: " + e); 
					}
					try {
						dis1 = new DataInputStream(fin1);
					}
					catch (Exception e) {
						System.out.println("Greska: " + e); 
					}
					try {
						while ((linija1 = dis1.readLine()) != null) {//WHILE2
							try {
								upis.writeBytes(linija1 + "\n");
							}
							catch (IOException e) {
								System.out.println("Greska: " + e);
							}
						}//WHILE2
					}
					catch (IOException e) {
						System.out.println("Greska: " + e);
					}
					try {
						upis.writeBytes("\n.\n");
                      citanje.read(trt);
                      odg = new String(trt);            
                      System.out.println("DATA:" + odg);
						upis.writeBytes("QUIT\n");
                      citanje.read(trt);
                      odg = new String(trt);            
                      System.out.println("QUIT:" + odg);
					}
					catch (IOException e) {
						System.out.println("Greska: " + e);
					}
					try {
						Thread.sleep(700);
					}
					catch (InterruptedException e) {
					}
				}//WHILE1
			}//try-WHILE1
			catch (IOException e) {
				System.out.println("Greska: " + e);
			}
		}//IF
		try {
			upis.close();
			citanje.close();
			fin.close();
			fin1.close();
			dis.close();
			dis1.close();
			klijent.close();
		}
		catch (IOException e) {
			System.out.println("Greska: " + e);
			System.out.println("Zavrsio sam");
		}
	}//MAIN
}//CLASS

