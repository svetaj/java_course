import java.io.*;

public class KomandaOS {

    public static void main(String[] args) {

		Process child = null;

	    String inputLine, outputLine;

		try {
			String [] cmdLine=new String [3];
			cmdLine[0]="cmd.exe";
			cmdLine[1]="/C";
            cmdLine[2]="c:\\komanda.bat";

        	child = Runtime.getRuntime().exec(cmdLine);
	     	BufferedReader input = new BufferedReader (new InputStreamReader(child.getInputStream()));
			while ((outputLine = input.readLine()) != null) {
	    		System.out.println(outputLine);
			}
     		input.close();
     	}
   		 catch (Exception err) {
    		 err.printStackTrace();
     	}

    }
}
