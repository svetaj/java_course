// <applet code=Raspored3 width=300 height=250></applet>
import javax.swing.*;
import java.awt.*;

public class Raspored3 extends JApplet {
  public void init() {
    Container cp = getContentPane();
    cp.setLayout(new GridLayout(7,3));
    for(int i = 0; i < 20; i++)
      cp.add(new JButton("Button " + i));
  }
}
