//: c14:InvokeLaterFrame.java
// Eliminating race Conditions using Swing Components.
// From 'Thinking in Java, 3rd ed.' (c) Bruce Eckel 2002
// www.BruceEckel.com. See copyright notice in CopyRight.txt.
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class InvokeLaterFrame extends JFrame {
  private JTextField statusField =
    new JTextField("Initial Value");
  public InvokeLaterFrame() {
    Container cp = getContentPane();
    cp.add(statusField, BorderLayout.NORTH);
    addWindowListener(new WindowAdapter() {
      public void windowOpened(WindowEvent e) {
        try { // Simulate initialization overhead
          Thread.sleep(2000);
        } catch (InterruptedException ex) {
           throw new RuntimeException(ex);
        }
        statusField.setText("Initialization complete");
      }
    });
  }
} ///:~
