//<applet code=Raspored1 width=300 height=250></applet>
import javax.swing.*;
import java.awt.*;

public class Raspored1 extends JApplet {
  public void init() {
    Container cp = getContentPane();
    cp.add(BorderLayout.NORTH, new JButton("Gore"));
    cp.add(BorderLayout.SOUTH, new JButton("Dole"));
    cp.add(BorderLayout.EAST, new JButton("Levo"));
    cp.add(BorderLayout.WEST, new JButton("Desno"));
    cp.add(BorderLayout.CENTER, new JButton("Centar"));
  }
} ///:~
