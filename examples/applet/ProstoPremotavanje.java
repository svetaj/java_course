/*
 * 1.0 kod (isti i u 1.1).
 */

import java.applet.Applet;
import java.awt.TextField;

public class ProstoPremotavanje extends Applet {

    TextField field;

    public void init() {
        //kreira tekstualno polje i zabranuje editovanje.
        field = new TextField();
        field.setEditable(false);

        //Set the layout manager so that the text field will be
        //as wide as possible.
        setLayout(new java.awt.GridLayout(1,0));

        //dodaje tekstualno polje u applet.
        add(field);
        validate();  //ovo nije neophodno

        addItem("inicijalizovanje... ");
    }

    public void start() {
        addItem("pokretanje... ");
    }

    public void stop() {
        addItem("zaustavljanje... ");
    }

    public void destroy() {
        addItem("priprema za izbacivanje iz memorije...");
    }

    void addItem(String newWord) {
        String t = field.getText();
        System.out.println(newWord);
        field.setText(t + newWord);
    }
}
