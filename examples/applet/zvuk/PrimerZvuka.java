import java.applet.*;
import java.awt.*;

public class PrimerZvuka extends Applet {
    ZvukLista soundList;
    String onceFile = "avav.au";
    String loopFile = "voz.au";
    AudioClip onceClip;
    AudioClip loopClip;

    Button playOnce;
    Button startLoop;
    Button stopLoop;
    Button reload;

    boolean looping = false;

    public void init() {
        playOnce = new Button("Av av!");
        add(playOnce);

        startLoop = new Button("Zvucna petlja start");
        stopLoop = new Button("Zvucna petlja stop");
        stopLoop.disable();
        add(startLoop);
        add(stopLoop);

        reload = new Button("Ponovo ucitaj zvukove");
        add(reload);

        validate();

        startLoadingSounds();
    }

    void startLoadingSounds() {
        //Start asynchronous sound loading.
        soundList = new ZvukLista(this, getCodeBase());
        soundList.startLoading(loopFile);
        soundList.startLoading(onceFile);
   }

    public void stop() {
        if (looping) {
            loopClip.stop();
        }
    }

    public void start() {
        if (looping) {
            loopClip.loop();
        }
    }

    public boolean action(Event event, Object arg) {
        if (event.target == playOnce) {
            if (onceClip == null) {
                onceClip = soundList.getClip(onceFile);
            }

            if (onceClip != null) {
                onceClip.play();
                showStatus("Reprodukuje zvuk " + onceFile + ".");
            } else {
                showStatus("Zvuk " + onceFile + " nije jos ucitan.");
            }
            return true;
        }

        if (event.target == startLoop) {
            if (loopClip == null) {
                loopClip = soundList.getClip(loopFile);
            }

            if (loopClip != null) {
                looping = true;
                loopClip.loop();
                stopLoop.enable();
                startLoop.disable();
                showStatus("Reprodukuje zvuk " + loopFile + " neprestano.");
            } else {
                showStatus("Zvuk " + loopFile + " nije jos ucitan.");
            }
            return true;
        }

        if (event.target == stopLoop) {
            if (looping) {
                looping = false;
                loopClip.stop();
                startLoop.enable();
                stopLoop.disable();
            }
            showStatus("Zaustavljeno reprodukovanje " + loopFile + ".");
            return true;
        }

        if (event.target == reload) {
            if (looping) {
                looping = false;
                loopClip.stop();
                startLoop.enable();
                stopLoop.disable();
            }
            loopClip = null;
            onceClip = null;
            startLoadingSounds();
            showStatus("Reprodukuje sve zvuke");
            return true;
        }

        return false;
    }
}