import java.applet.*;
import java.net.URL;

class UcitavacZvuka extends Thread {
    Applet applet;
    ZvukLista soundList;
    URL baseURL;
    String relativeURL;

    public UcitavacZvuka(Applet applet, ZvukLista soundList,
                       URL baseURL, String relativeURL) {
        this.applet = applet;
        this.soundList = soundList;
        this.baseURL = baseURL;
        this.relativeURL = relativeURL;
        setPriority(MIN_PRIORITY);
        start();
    }

    public void run() {
        AudioClip audioClip = applet.getAudioClip(baseURL, relativeURL);

        try {
            sleep((int)(Math.random()*10000));
        } catch (InterruptedException e) {}

        soundList.putClip(audioClip, relativeURL);
    }
}