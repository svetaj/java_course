import java.applet.*;
import java.net.URL;

class ZvukLista extends java.util.Hashtable {
    Applet applet;
    URL baseURL;

    public ZvukLista(Applet applet, URL baseURL) {
        super(5);
        this.applet = applet;
        this.baseURL = baseURL;
    }

    public void startLoading(String relativeURL) {
        new UcitavacZvuka(applet, this,
                        baseURL, relativeURL);
    }

    public AudioClip getClip(String relativeURL) {
        return (AudioClip)get(relativeURL);
    }

    public void putClip(AudioClip clip, String relativeURL) {
        put(relativeURL, clip);
    }
}