/*
 * 1.1 verzija.
 */

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.net.MalformedURLException;

public class PrikazDokumenta extends Applet
                          implements ActionListener {
    URLWindow urlWindow;

    public void init() {
        Button button = new Button("Podize URL prozor");
        button.addActionListener(this);
        add(button);

        urlWindow = new URLWindow(getAppletContext());
        urlWindow.pack();
    }

    public void destroy() {
        urlWindow.setVisible(false);
        urlWindow = null;
    }

    public void actionPerformed(ActionEvent event) {
        urlWindow.setVisible(true);
    }
}

class URLWindow extends Frame
                implements ActionListener {
    TextField urlField;
    Choice choice;
    AppletContext appletContext;

    public URLWindow(AppletContext appletContext) {
        super("Prikaz dokumenta!");

        this.appletContext = appletContext;

        GridBagLayout gridBag = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        setLayout(gridBag);

        Label label1 = new Label("URL ili dokument koji treba da se prikaze:",
				 Label.RIGHT);
        gridBag.setConstraints(label1, c);
        add(label1);

        urlField = new TextField("http://www.b92.net/", 40);
        urlField.addActionListener(this);
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0;
        gridBag.setConstraints(urlField, c);
        add(urlField);

        Label label2 = new Label("Prozor/okvir koji treba da se prikaze:",
				 Label.RIGHT);
        c.gridwidth = 1;
        c.weightx = 0.0;
        gridBag.setConstraints(label2, c);
        add(label2);

        choice = new Choice();
        choice.addItem("(izbor pretrazivaca)"); //ne zadaje se
        choice.addItem("Moj licni prozor");     //prozor pod imenom
					                            //"Moj licni prozor"
        choice.addItem("_blank");               //novi, neimenovani prozor
        choice.addItem("_self");
        choice.addItem("_parent");
        choice.addItem("_top");                 //okvir koji sadrzi ovaj aplet
        c.fill = GridBagConstraints.NONE;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.anchor = GridBagConstraints.WEST;
        gridBag.setConstraints(choice, c);
        add(choice);

        Button button = new Button("Prikazuje dokument");
        button.addActionListener(this);
        c.weighty = 1.0;
        c.ipadx = 10;
        c.ipady = 10;
        c.insets = new Insets(5,0,0,0);
        c.anchor = GridBagConstraints.SOUTH;
        gridBag.setConstraints(button, c);
        add(button);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event) {
                setVisible(false);
            }
        });
    }

    public void actionPerformed(ActionEvent event) {
        String urlString = urlField.getText();
        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            System.err.println("Lose napravljen URL: " + urlString);
        }

        if (url != null) {
            if (choice.getSelectedIndex() == 0) {
                appletContext.showDocument(url);
            } else {
                appletContext.showDocument(url,
				  choice.getSelectedItem());
            }
        }
    }
}
