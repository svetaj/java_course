/*
 * 1.1 version.
 */

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;

public class Posiljalac extends Applet
                    implements ActionListener {
    private String myName;
    private TextField nameField;
    private TextArea status;
    private String newline;

    public void init() {
        GridBagLayout gridBag = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();

        setLayout(gridBag);

        Label receiverLabel = new Label("Ime primaoca:",
                                        Label.RIGHT);
        gridBag.setConstraints(receiverLabel, c);
        add(receiverLabel);

        nameField = new TextField(getParameter("RECEIVERNAME"),
                                               10);
        c.fill = GridBagConstraints.HORIZONTAL;
        gridBag.setConstraints(nameField, c);
        add(nameField);
        nameField.addActionListener(this);

        Button button = new Button("Posalji poruku");
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.anchor = GridBagConstraints.WEST;

        c.fill = GridBagConstraints.NONE;

        gridBag.setConstraints(button, c);
        add(button);
        button.addActionListener(this);

        status = new TextArea(5, 60);
        status.setEditable(false);
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 1.0;
        gridBag.setConstraints(status, c);
        add(status);

        myName = getParameter("NAME");
        Label senderLabel = new Label("(Moje ime je " + myName + ".)",
                                      Label.CENTER);
        c.weightx = 0.0;
        c.weighty = 0.0;
        gridBag.setConstraints(senderLabel, c);
        add(senderLabel);

	newline = System.getProperty("line.separator");
    }

    public void actionPerformed(ActionEvent event) {
        Applet receiver = null;
        String receiverName = nameField.getText();

        receiver = getAppletContext().getApplet(receiverName);
        if (receiver != null) {
            if (!(receiver instanceof Primalac)) {
                status.append("Pronasao applet "
                              + receiverName + ", "
                              + "ali nije objekat Primalac."
			      + newline);
            } else {
                status.append("Nasao applet pod imenom "
                              + receiverName + newline
                              + "  Saljem mu poruku."
			      + newline);
                ((Primalac)receiver).processRequestFrom(myName);
            }
        } else {
            status.append("Ne mogu da nadjem applet "
                          + receiverName + "." + newline);
        }
    }

    public Insets getInsets() {
        return new Insets(3,3,3,3);
    }

    public void paint(Graphics g) {
        g.drawRect(0, 0,
                   getSize().width - 1, getSize().height - 1);
    }

    public String getAppletInfo() {
        return "Posiljalac, autor Sveta";
    }
}
