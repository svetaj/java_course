// <applet code=Dugme2 width=200 height=75></applet>
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class Dugme2 extends JApplet {
  private JButton
    b1 = new JButton("Dugme 1"),
    b2 = new JButton("Dugme 2");
  private JTextField txt = new JTextField(10);
  class ButtonListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
      String name = ((JButton)e.getSource()).getText();
      txt.setText(name);
    }
  }
  private ButtonListener bl = new ButtonListener();
  public void init() {
    b1.addActionListener(bl);
    b2.addActionListener(bl);
    Container cp = getContentPane();
    cp.setLayout(new FlowLayout());
    cp.add(b1);
    cp.add(b2);
    cp.add(txt);
  }
} ///:~
