/*
 * 1.0 code.
 */

import java.applet.*;
import java.awt.*;

public class Primalac extends Applet {
    private final String waitingMessage = "Cekam poruku...           ";
    private Label label = new Label(waitingMessage, Label.RIGHT);

    public void init() {
        add(label);
        add(new Button("Ocisti"));
        add(new Label("(Moje ime je " + getParameter("name") + ".)",
                      Label.LEFT));
        validate();
    }

    public boolean action(Event event, Object o) {
        label.setText(waitingMessage);
        repaint();
        return false;
    }

    public void processRequestFrom(String senderName) {
        label.setText("Primljena poruka od " + senderName + "!");
        repaint();
    }

    public void paint(Graphics g) {
        g.drawRect(0, 0, size().width - 1, size().height - 1);
    }

    public String getAppletInfo() {
        return "Primalac (imenovan " + getParameter("name") + "), autor Sveta";
    }
}

