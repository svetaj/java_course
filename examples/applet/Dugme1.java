// <applet code=Dugme1 width=200 height=50></applet>
import javax.swing.*;
import java.awt.*;

public class Dugme1 extends JApplet {
  private JButton
    b1 = new JButton("Dugme 1"),
    b2 = new JButton("Dugme 2");
  public void init() {
    Container cp = getContentPane();
    cp.setLayout(new FlowLayout());
    cp.add(b1);
    cp.add(b2);
  }
} ///:~