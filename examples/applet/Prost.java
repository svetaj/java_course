/*
 * 1.0 java kod.
 */

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Event;

public class Prost extends Applet {

    StringBuffer buffer;

    public void init() {
	buffer = new StringBuffer();
        addItem("inicijalizovanje... ");
    }

    public void start() {
        addItem("pokretanje... ");
    }

    public void stop() {
        addItem("zaustavljanje... ");
    }

    public void destroy() {
        addItem("pripremanje za izbacivanje iz memorije...");
    }

    void addItem(String newWord) {
        System.out.println(newWord);
        buffer.append(newWord);
        repaint();
    }

	public boolean mouseDown(Event event, int x, int y) {
    	addItem("klik!... ");
    	return true;
	}

    public void paint(Graphics g) {
	//Crta pravougaonik oko oblasti prikaza apleta.
        g.drawRect(0, 0, size().width - 1, size().height - 1);

	//Crta aktuelni string unutar pravougaonika.
        g.drawString(buffer.toString(), 5, 15);
    }
}

